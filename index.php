<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require '.maintenance.php';

// absolute filesystem path to this web root
define('WWW_DIR', __DIR__);

// absolute filesystem path to the application root
define('APP_DIR', WWW_DIR . '/app');

// absolute filesystem path to the composer dir
define('VENDOR_DIR',  WWW_DIR . '/vendor');

//debug mode
define('DEBUG', FALSE);

$container = require __DIR__ . '/app/bootstrap.php';

$container->getByType('Nette\Application\Application')->run();
