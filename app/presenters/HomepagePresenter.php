<?php

namespace App\Presenters;

use Nette;

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter {

    /**
     * @var \Model\Calculation\Base\Junction
     * @inject
     */
    public $mJunction;

    /**
     * @var \Model\Calculation\Base\ManualCorrection
     * @inject
     */
    public $mManualCorrection;

    public function renderDefault() {
            $this->template->result = $this->mJunction->initCron();
//        $this->mManualCorrection->moneyCorrection();
//        $this->mManualCorrection->opravPoctyHracu();
//        $this->mManualCorrection->pregenerujRadaRealna();
//        $this->mManualCorrection->opravCasy();
    }

}
