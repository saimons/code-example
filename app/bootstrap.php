<?php

require VENDOR_DIR . '/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
$configurator->enableDebugger(WWW_DIR . '/log');

$configurator->setTempDirectory(WWW_DIR . '/temp');

$configurator->createRobotLoader()
        ->addDirectory(APP_DIR)
        ->addDirectory(VENDOR_DIR)
//        ->addDirectory('/home/smanager/public_html/stadionmanager/app/Model')
        ->addDirectory('/xampp/htdocsSM/app/Model') //localhost
        ->register();

$configurator->addConfig(APP_DIR . '/config/config.neon');
$configurator->addConfig(APP_DIR . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
