<?php

namespace Model\Calculation\Base;

use Nette\Object,
    Model\Calculation\Match\Match;

/**
 * Model pro inicializaci CRONU
 */
class Junction extends Object {
      
    
    /**
     * @var \Model\Calculation\Match\Match
     */
    private $calculationMatch;
    
    

    /**
     * 
     * @param \AModel\Calculation\Match\Match $calculationMatch
     */
    public function __construct(Match $calculationMatch) {
       $this->calculationMatch = $calculationMatch;   
    }

    public function initCron() {
        $this->calculationMatch->run();        
        return 'Done';
    }
    
}
