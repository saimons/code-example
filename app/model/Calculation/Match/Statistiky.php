<?php

namespace Model\Calculation\Match;

use Nette\Object;

/**
 * statistiky zapasu
 *
 * @author stepan
 */
class Statistiky extends Object {

    /**
     * Pole pro udalosti ve hre
     * @var array
     */
    private $udalost = array();

    /**
     * Pole pro formaci ktera hrala
     * @var array
     */
    private $udalostsestava = array();
    
    /**
     * Pole pro taktiky tymu
     * @var array
     */
    private $udalosttaktika = array();

    /**
     * @var UtkaniPrubeh
     */
    private $utkaniPrubeh;

    /**
     * @var \Nette\Database\Table\Selection
     */
    private $utkani;

    /**
     * @var array
     */
    private $hvezdyBrankar = array();

    /**
     * @var array
     */
    private $hvezdy = array();

    /**
     * @var array
     */
    private $pocetGolu = array('domaci' => 0, 'hoste' => 0);

    /**
     * @var array
     */
    private $pocetAsistenci = array('domaci' => 0, 'hoste' => 0);

    /**
     * @param \Model\Calculation\Match\UtkaniPrubeh $utkaniPrubeh
     * @param \Nette\Database\Table\Selection $utkani
     */
    public function __construct(UtkaniPrubeh $utkaniPrubeh, $utkani) {
        $this->utkaniPrubeh = $utkaniPrubeh;
        $this->utkani = $utkani;
    }

    /**
     * Vlozi udalost do statistik
     * @param int $hokejista_id
     * @param int $typudalost_id
     * @param bool $domaciHoste
     */
    public function setUdalost($hokejista_id, $typudalost_id, $domaciHoste) {
        
//        $ctvrtaFormace = array(375716, 118715, 473235, 331624, 368473);
//        if (in_array($hokejista_id, $ctvrtaFormace)) {
//            dump('Hrjeeeee ' . $hokejista_id . '; typudalost ' . $typudalost_id . '; cas' . gmdate("H:i:s", $this->utkaniPrubeh->getCasUtkani()));
//            die();
//        }
        
        
        //goly
        $golTypudalost = array(1, 2, 3, 11, 12, 13, 14, 15, 16);
        $asistenceTypudalost = array(4, 19);

        $this->udalost[] = array('hokejista_id' => $hokejista_id, 'typudalost_id' => $typudalost_id, 'utkani_id' => $this->utkani->id, 'casZapas' => gmdate("H:i:s", $this->utkaniPrubeh->getCasUtkani()), 'domaciHoste' => $domaciHoste);

        if (in_array($typudalost_id, $golTypudalost)) { //hokejista            
            isset($this->hvezdy[$hokejista_id]['gol']) ? $this->hvezdy[$hokejista_id]['gol'] ++ : $this->hvezdy[$hokejista_id]['gol'] = 1;
            $this->hvezdy[$hokejista_id]['domaciHoste'] = $domaciHoste;
            $domaciHoste ? $this->pocetGolu['domaci'] ++ : $this->pocetGolu['hoste'] ++;
        } elseif (in_array($typudalost_id, $asistenceTypudalost)) {
            isset($this->hvezdy[$hokejista_id]['asistence']) ? $this->hvezdy[$hokejista_id]['asistence'] ++ : $this->hvezdy[$hokejista_id]['asistence'] = 1;
            $this->hvezdy[$hokejista_id]['domaciHoste'] = $domaciHoste;
            $domaciHoste ? $this->pocetAsistenci['domaci'] ++ : $this->pocetAsistenci['hoste'] ++;
        } elseif (in_array($typudalost_id, array(20, 21))) { //brankar
            isset($this->hvezdyBrankar[$hokejista_id]['strela']) ? $this->hvezdyBrankar[$hokejista_id]['strela'] ++ : $this->hvezdyBrankar[$hokejista_id]['strela'] = 1; //strela
            $this->hvezdyBrankar[$hokejista_id]['domaciHoste'] = $domaciHoste;
            if ($typudalost_id == 21) { //gol
                isset($this->hvezdyBrankar[$hokejista_id]['gol']) ? $this->hvezdyBrankar[$hokejista_id]['gol'] ++ : $this->hvezdyBrankar[$hokejista_id]['gol'] = 1;
            }
        }
    }

    /**
     * Vlozi udalost do statistik pro sestavu
     * @param int $hokejista_id
     * @param int $typudalost_id
     * @param bool $domaciHoste
     * @param int $hodnota
     */
    public function setUdalostsestava($hokejista_id, $typudalost_id, $domaciHoste, $hodnota = NULL) {
        $this->udalostsestava[] = array('hokejista_id' => $hokejista_id, 'typudalost_id' => $typudalost_id, 'utkani_id' => $this->utkani->id, 'domaciHoste' => $domaciHoste, 'hodnota' => $hodnota, 'energie' => 0);
    }

    /**
     * Vrati vsechny udalosti
     * @return array
     */
    public function getUdalost() {
        return $this->udalost;
    }

    /**
     * Vrati vsechny sestavy odehrane
     * @return array
     */
    public function getUdalostsestava() {
        return $this->udalostsestava;
    }

    /**
     * Vygneruje tri hrace hvezdou zpasu
     */
    public function zjistiHvezdaZapasu() {
        $navrhHvezd = array();
        foreach ($this->hvezdyBrankar AS $hokejista_id => $h) {
            if ($h['strela'] > 10) { //pokud na brankare slo vice jak deset strel
                if (((isset($h['gol']) ? $h['gol'] : 0) / $h['strela']) < 0.05) { //pokud brankar chytal lepe nez 95% 
                    $navrhHvezd[] = array('hokejista_id' => $hokejista_id, 'domaciHoste' => $h['domaciHoste']); //je navrhnut do vyberu pro oceneni
                }
            }
        }

        $body = array();

        //vypocita se bodove ohodnoceni hracu, asistence gol
        foreach ($this->hvezdy AS $hokejista_id => $h) {
            $body[$hokejista_id] = (isset($h['gol']) ? $h['gol'] : 0) * 1.5 + (isset($h['asistence']) ? $h['asistence'] : 0);
        }

        arsort($body); //setridi se podle nejvice dosazenuych bodu
        $i = 4;
        //vyberou se 4 nejlepsi
        foreach ($body AS $hokejista_id => $body) {
            $navrhHvezd[] = array('hokejista_id' => $hokejista_id, 'domaciHoste' => $this->hvezdy[$hokejista_id]['domaciHoste']);
            if (!$i) {
                break;
            }
            $i--;
        }

        //z nominace na oceneni se nahodne vezmou tri hraci
        do {
            unset($navrhHvezd[rand(0, count($navrhHvezd))]);
        } while (count($navrhHvezd) > 3);

        //vlozi se do udalosti
        foreach ($navrhHvezd As $hvezdy) {
            $this->udalost[] = array('hokejista_id' => $hvezdy['hokejista_id'], 'typudalost_id' => 7, 'utkani_id' => $this->utkani->id, 'casZapas' => gmdate("H:i:s", 4320), 'domaciHoste' => $hvezdy['domaciHoste']);
        }
    }
    
    
    /**
     * Vrati pole pro ulozeni statistik golmanu
     * @return array
     */
    public function getStatisikyGolmani() {
        $statistikygolmani = array();
        foreach ($this->hvezdyBrankar AS $hokejista_id => $h) {
            $statistikygolmani[] = array('hokejista_id' => $hokejista_id, 'typutkani_id' => $this->utkaniPrubeh->getTyputkani_id(), 'pocetstrel' => $h['strela'], 'pocetgolu' => isset($h['gol']) ? $h['gol'] : 0);
        }
        return $statistikygolmani;
    }
    
    /**
     * Prida do udalost sestava spotrebovanou energii
     * @param int $hokejista_id
     * @param int $energie
     */
    public function updateHokejistaEnergie($hokejista_id, $energie) {
        
        foreach ($this->udalostsestava AS $key => $us) {
            //\Tracy\Debugger::barDump($this->udalostsestava[$key]['energie']);
            if ($us['hokejista_id'] == $hokejista_id AND $us['typudalost_id'] >= 100 AND $us['typudalost_id'] <= 121) {
                $this->udalostsestava[$key]['energie'] += $energie;
                break;
            }
        }
    }
    

}
