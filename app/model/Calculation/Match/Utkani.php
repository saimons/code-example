<?php

namespace Model\Calculation\Match;

use Nette\Object;

/**
 * Model pro vypocet zapasu
 */
class Utkani extends Object {

    const PR_VYLOUCENI_1 = 0.91, //pravdepodobnost vylouceni 1 hrace
            PR_VYLOUCENI_2 = 0.95, //pravdepodbnost vylouceni 2 hrace
            PR_TREST_120 = 0, //pravdepodobnost vylouceni na 120 s je 80%
            PR_TREST_240 = 0.9, //..
            PR_TREST_300 = 0.95, //..
            PR_STRELA = 0.95; //pravdepodobnost skonceni akce strelou 
    const TREST_120 = 120, //tresty za vylouceni v sec
            TREST_240 = 240,
            TREST_300 = 300;

    /**
     * @var \Nette\Database\Table\Selection
     */
    private $utkani;

    /**
     * @var Tym
     */
    private $tymTomaci;

    /**
     * @var Tym
     */
    private $tymHoste;

    /**
     * @var UtkaniPrubeh
     */
    private $utkaniPrubeh;

    /**
     * Aktualni formace ktera hraje
     * @var ZakladniFormace | PresilovkaFormace | OslabeniFormace | NajezdyFormace
     */
    private $sestavaD;

    /**
     * Aktualni formace ktera hraje
     * @var ZakladniFormace | PresilovkaFormace | OslabeniFormace | NajezdyFormace
     */
    private $sestavaH;

    /**
     * @var Vylouceni
     */
    private $vylouceni;

    /**
     * @var Statistiky
     */
    private $statistiky;

    /**
     * @var array
     */
    private $zraneni = array();

    /**
     * @param \Nette\Database\Table\Selection $utkani
     * @param Tym $tymTomaci
     * @param Tym $tymHoste
     */
    public function __construct($utkani, $tymTomaci, $tymHoste) {
        $this->utkani = $utkani;
        $this->tymTomaci = $tymTomaci;
        $this->tymHoste = $tymHoste;
        $this->utkaniPrubeh = new UtkaniPrubeh();
        $this->vylouceni = new Vylouceni();
        $this->statistiky = new Statistiky($this->utkaniPrubeh, $this->utkani);

        $this->tymTomaci->setUtkaniPrubeh($this->utkaniPrubeh);
        $this->tymHoste->setUtkaniPrubeh($this->utkaniPrubeh);
        $this->utkaniPrubeh->setTyputkani_id($this->utkani->typutkani_id);
    }

    /**
     * Spusteni vypoctu
     */
    public function run() {

        //ulozeni sestavy do DB
        $this->nastavUdalostsestava();

        //hlavni smycka
        $this->hlavniSmyckaZapasu();

        //hvezdy zapasu
        $this->statistiky->zjistiHvezdaZapasu();

        //energie, zkusenosti
        $this->updateHokejistaPoZapase();

        //zraneni hokejistu
        $this->zraneniHokejistu();

        //vysledek zapas
        $this->ulozVysledekZapasu();

        //ulozi do sestava v kolika formacich hral tym v jednotlivych tretinach
        $this->statistkyPoTretinach();

    }

    /**
     * Hlavni smycka prepocty
     */
    private function hlavniSmyckaZapasu() {
        $this->casUtkaniInit(); //incializace casu na zacatku
        while ($this->utkaniPrubeh->getCasUtkani() < $this->utkaniPrubeh->getMaxCasUtkani()) {

            if ($this->utkaniPrubeh->getGolyDomaci() != $this->utkaniPrubeh->getGolyHoste() AND $this->utkaniPrubeh->getCasUtkani() >= 3601) { //zapas skoncil v normalni hraci dobe
                break;
            } elseif ($this->utkaniPrubeh->getGolyDomaci() == $this->utkaniPrubeh->getGolyHoste() AND $this->utkaniPrubeh->getCasUtkani() >= 3601) { //prodlouzeni
                $this->utkaniPrubeh->setProdlouzeni();
            }

            $this->stavVylouceni();
            $this->sestava();
            $this->vylouceni();
            $this->utocnaAkce();
            $this->casUtkani();
        }

        //samostatne najezdy
        if ($this->utkaniPrubeh->getGolyDomaci() == $this->utkaniPrubeh->getGolyHoste()) {
            $this->utkaniPrubeh->setNajezdy();
            $this->samostatneNajezdy();
        }
    }

    /**
     * Meni se rotace aktualni sestavy, pokud je vylouceni vola se metoda ktera rotuje sestavy pro presilovky
     */
    private function sestava() {
        $casLedZDCelkem = 0;
        $casLedZHCelkem = 0;

        //pridat reakci na tri formace
        for ($i = 1; $i <= 4; $i++) {
            if ($this->tymTomaci->getPocetLajn() == 3 AND $i == 4) { //pokud se ma hrat na tri formace
                $casLedZD[$i] = 0;  
            } else {
                $casLedZD[$i] = $this->tymTomaci->getZakladniFormace($i)->getCasLed();
            }
            $casLedZDCelkem += $casLedZD[$i];
        }
        for ($i = 1; $i <= 4; $i++) {
            if ($this->tymHoste->getPocetLajn() == 3 AND $i == 4) { //pokud se ma hrat na tri formace
                $casLedZH[$i] = 0;
            } else {
                $casLedZH[$i] = $this->tymHoste->getZakladniFormace($i)->getCasLed();
            }
            $casLedZHCelkem += $casLedZH[$i];
        }


        //zjisti se jaka formace hraje
        $otockaD = floor($this->utkaniPrubeh->getCasRotace() / $casLedZDCelkem);
        $otockaH = floor($this->utkaniPrubeh->getCasRotace() / $casLedZHCelkem);
        
        //pokud neni vylouceni
        if (!$this->vylouceni->isVylouceni()->domaci AND ! $this->vylouceni->isVylouceni()->hoste) {
            //zvoli se podle casu spravna formace do sestavaD
            if (($this->utkaniPrubeh->getCasRotace() >= $otockaD * ($casLedZDCelkem)) AND ( $this->utkaniPrubeh->getCasRotace() <= $casLedZD[1] + $otockaD * ($casLedZDCelkem))) {
                $this->sestavaD = $this->tymTomaci->getZakladniFormace(1);
            } elseif (($this->utkaniPrubeh->getCasRotace() > $casLedZD[1] + $otockaD * ($casLedZDCelkem)) AND ( $this->utkaniPrubeh->getCasRotace() <= $casLedZD[1] + $casLedZD[2] + $otockaD * ($casLedZDCelkem))) {
                $this->sestavaD = $this->tymTomaci->getZakladniFormace(2);
            } elseif (($this->utkaniPrubeh->getCasRotace() > $casLedZD[1] + $casLedZD[2] + $otockaD * ($casLedZDCelkem)) AND ( $this->utkaniPrubeh->getCasRotace() <= $casLedZD[1] + $casLedZD[2] + $casLedZD[3] + $otockaD * ($casLedZDCelkem))) {
                $this->sestavaD = $this->tymTomaci->getZakladniFormace(3);                
            } else {
                $this->sestavaD = $this->tymTomaci->getZakladniFormace(4);
            }            
            
            //zvoli se podle casu spravna formace do sestavaH
            if (($this->utkaniPrubeh->getCasRotace() >= $otockaH * ($casLedZHCelkem)) AND ( $this->utkaniPrubeh->getCasRotace() <= $casLedZH[1] + $otockaH * ($casLedZHCelkem))) {
                $this->sestavaH = $this->tymHoste->getZakladniFormace(1);
            } elseif (($this->utkaniPrubeh->getCasRotace() > $casLedZH[1] + $otockaH * ($casLedZHCelkem)) AND ( $this->utkaniPrubeh->getCasRotace() <= $casLedZH[1] + $casLedZH[2] + $otockaH * ($casLedZHCelkem))) {
                $this->sestavaH = $this->tymHoste->getZakladniFormace(2);
            } elseif (($this->utkaniPrubeh->getCasRotace() > $casLedZH[1] + $casLedZH[2] + $otockaH * ($casLedZHCelkem)) AND ( $this->utkaniPrubeh->getCasRotace() <= $casLedZH[1] + $casLedZH[2] + $casLedZH[3] + $otockaH * ($casLedZHCelkem))) {
                $this->sestavaH = $this->tymHoste->getZakladniFormace(3);
            } else {
                $this->sestavaH = $this->tymHoste->getZakladniFormace(4);
            }
        } else {
            $this->vylouceniSestava();
        }
    }

    /**
     * Rotuje presilivkove/oslabeni formace
     */
    private function vylouceniSestava() {
        $pocetVylouceniD = count($this->vylouceni->isVylouceni()->domaci);
        $pocetVylouceniH = count($this->vylouceni->isVylouceni()->hoste);

        $formace = ceil($this->utkaniPrubeh->getCasRotaceVylouceni() / 60) % 2 == 0 ? 2 : 1; //formace se meni po 60 sec
        //ruzne varianty presilovek
        if ($pocetVylouceniD == 0 AND $pocetVylouceniH == 1) { // 5:4
            $this->sestavaD = $this->tymTomaci->getPresilovkaFormace($formace);
            $this->sestavaH = $this->tymHoste->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->hoste);
        } elseif ($pocetVylouceniD == 1 AND $pocetVylouceniH == 0) { //4:5       
            $this->sestavaD = $this->tymTomaci->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->domaci);
            $this->sestavaH = $this->tymHoste->getPresilovkaFormace($formace);
        } elseif ($pocetVylouceniD == 1 AND $pocetVylouceniH == 1) { //4:4       
            $this->sestavaD = $this->tymTomaci->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->domaci);
            $this->sestavaH = $this->tymHoste->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->hoste);
        } elseif ($pocetVylouceniD == 0 AND $pocetVylouceniH == 2) { //5:3       
            $this->sestavaD = $this->tymTomaci->getPresilovkaFormace($formace);
            $this->sestavaH = $this->tymHoste->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->hoste, TRUE);
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 0) { //3:5       
            $this->sestavaD = $this->tymTomaci->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->domaci, TRUE);
            $this->sestavaH = $this->tymHoste->getPresilovkaFormace($formace);
        } elseif ($pocetVylouceniD == 1 AND $pocetVylouceniH == 2) { //4:3       
            $this->sestavaD = $this->tymTomaci->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->domaci);
            $this->sestavaH = $this->tymHoste->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->hoste, TRUE);
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 1) { //3:4       
            $this->sestavaD = $this->tymTomaci->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->domaci, TRUE);
            $this->sestavaH = $this->tymHoste->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->hoste);
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 2) { //3:3       
            $this->sestavaD = $this->tymTomaci->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->domaci, TRUE);
            $this->sestavaH = $this->tymHoste->getOslabeniFormace($formace, $this->vylouceni->isVylouceni()->hoste, TRUE);
        }
    }

    /**
     * Resi se cas zapasu a rotaci formaci, automaticky se iknrementuje a vznikaji zapasove udalosti
     */
    private function casUtkani() {
        //dump('poceeeet lajn'); dump(array('domaci' => $this->tymTomaci->getPocetLajn(), 'hoste' => $this->tymHoste->getPocetLajn()));        
        $this->utkaniPrubeh->createDalsiUdalost();
        $this->utkaniPrubeh->incrementCas();
        
        //reakce na tri formace
        if ($this->tymTomaci->getTaktika()->getTriFormaceReakce()) {
            if ($this->tymHoste->getPocetLajn() == 3) {
                $lajnD = 3;
            } else {
                $lajnD = $this->tymTomaci->getPocetLajn();
            }
        } else {
            $lajnD = $this->tymTomaci->getPocetLajn();
        }
        
        if ($this->tymHoste->getTaktika()->getTriFormaceReakce()) {
            if ($this->tymTomaci->getPocetLajn() == 3) {
                $lajnH = 3;
            } else {
                $lajnH = $this->tymHoste->getPocetLajn();
            }
        } else {
            $lajnH = $this->tymHoste->getPocetLajn();
        }
        
        $this->utkaniPrubeh->setPocetLajn(array('domaci' => $lajnD, 'hoste' => $lajnH));
        $this->utkaniPrubeh->setBrankarLajna(array('domaci' => $this->tymTomaci->getAktualiPoziceBrankar(), 'hoste' => $this->tymHoste->getAktualiPoziceBrankar()));
        $this->utkaniPrubeh->zmenyPravidla();
    }

    /**
     * nicializace na zacatku prepoctu casu a sestav
     */
    private function casUtkaniInit() {
        //reakce na tri formace
        if ($this->tymTomaci->getTaktika()->getTriFormaceReakce()) {
            if ($this->tymHoste->getPocetLajn() == 3) {
                $lajnD = 3;
            } else {
                $lajnD = $this->tymTomaci->getPocetLajn();
            }
        } else {
            $lajnD = $this->tymTomaci->getPocetLajn();
        }
        
        if ($this->tymHoste->getTaktika()->getTriFormaceReakce()) {
            if ($this->tymTomaci->getPocetLajn() == 3) {
                $lajnH = 3;
            } else {
                $lajnH = $this->tymHoste->getPocetLajn();
            }
        } else {
            $lajnH = $this->tymHoste->getPocetLajn();
        }
        
        $this->utkaniPrubeh->setPocetLajn(array('domaci' => $lajnD, 'hoste' => $lajnH));
        $this->utkaniPrubeh->setBrankarLajna(array('domaci' => $this->tymTomaci->getAktualiPoziceBrankar(), 'hoste' => $this->tymHoste->getAktualiPoziceBrankar()));
        $this->utkaniPrubeh->incrementCas();
    }

    /**
     * Je nejaky hrac vyloucen
     */
    private function stavVylouceni() {
        if (is_object($this->vylouceni->isVylouceni())) { //pokud je vylouceni
            if ($this->vylouceni->decremetVylouceni($this->utkaniPrubeh->getDalsiUdalost())) {
                $this->utkaniPrubeh->setCasRotace(0);
            }
        }
    }

    /**
     * Generuje se nahoda pro vylouceni
     */
    private function vylouceni() {
        $pocetVylouceniD = count($this->vylouceni->isVylouceni()->domaci);
        $pocetVylouceniH = count($this->vylouceni->isVylouceni()->hoste);

        //jestli dojde k vylouceni a o kolikateho se jedna hrace
        if ($pocetVylouceniD == 0 AND $pocetVylouceniH == 0) {
            if (self::nahoda() > self::PR_VYLOUCENI_1) {
                $this->vylucHrace('zakladni');
            }
        } elseif (($pocetVylouceniD == 0 AND $pocetVylouceniH == 1) OR ( $pocetVylouceniD == 1 AND $pocetVylouceniH == 0) OR ( $pocetVylouceniD == 1 AND $pocetVylouceniH == 1)) { // 5:4
            if (self::nahoda() > self::PR_VYLOUCENI_2) {
                $this->vylucHrace('zakladni');
            }
        } elseif ($pocetVylouceniD == 0 AND $pocetVylouceniH == 2) {
            if (self::nahoda() > self::PR_VYLOUCENI_2) {
                $this->vylucHrace('zakladni', 1);
            }
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 0) {
            if (self::nahoda() > self::PR_VYLOUCENI_2) {
                $this->vylucHrace('zakladni', 0);
            }
        }
    }

    /**
     * Vylouci konkretniho hrace
     * @param strong $pozice
     * @param int $hosteDomaciPouze 0-hoste,1-domaci,5
     */
    private function vylucHrace($pozice, $hosteDomaciPouze = 5) {
        $sance = $this->sestavaD->getParametryVylouceni() / ($this->sestavaD->getParametryVylouceni() + $this->sestavaH->getParametryVylouceni());

        //kdo je vyloucen
        if (self::nahoda() <= $sance) { // Hoste
            $sestava = $this->sestavaH;
            $hosteDomaci = 0;
        } else { //Domace
            $sestava = $this->sestavaD;
            $hosteDomaci = 1;
        }

        if ($hosteDomaciPouze == 5 OR $hosteDomaci == $hosteDomaciPouze) { //pokud jsou dva vylouceni vylucuje se pouze na jedne strasne
            $nahoda = self::nahoda();
            $trest = self::casTrest();

            //jaka pozice je vyloucena
            if ($nahoda <= ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani / $sestava->getParametryVylouceni())) {//vylouceni RW
                $hokejista = $sestava->getHracPozice(new Pozice($pozice, 'RW'));
                if ($hokejista->id != 0) {
                    $this->vylouceni->setVylouceni($hokejista, $trest->cas, $hosteDomaci);
                    $this->utkaniPrubeh->setCasRotaceVylouceni(0);
                }
            } elseif ($nahoda > ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani / $sestava->getParametryVylouceni())
                    AND $nahoda <= ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'C'))->sebeovladani) / $sestava->getParametryVylouceni()) { // C
                $hokejista = $sestava->getHracPozice(new Pozice($pozice, 'C'));
                if ($hokejista->id != 0) {
                    $this->vylouceni->setVylouceni($hokejista, $trest->cas, $hosteDomaci);
                    $this->utkaniPrubeh->setCasRotaceVylouceni(0);
                }
            } elseif (($nahoda > ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'C'))->sebeovladani) / $sestava->getParametryVylouceni())
                    AND ( $nahoda <= ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'C'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'LW'))->sebeovladani) / $sestava->getParametryVylouceni())) { // LW
                $hokejista = $sestava->getHracPozice(new Pozice($pozice, 'LW'));
                if ($hokejista->id != 0) {
                    $this->vylouceni->setVylouceni($hokejista, $trest->cas, $hosteDomaci);
                    $this->utkaniPrubeh->setCasRotaceVylouceni(0);
                }
            } elseif (($nahoda > ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'C'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'LW'))->sebeovladani) / $sestava->getParametryVylouceni())
                    AND ( $nahoda <= ($sestava->getHracPozice(new Pozice($pozice, 'RW'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'C'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'LW'))->sebeovladani + $sestava->getHracPozice(new Pozice($pozice, 'RD'))->sebeovladani) / $sestava->getParametryVylouceni())) { // RD
                $hokejista = $sestava->getHracPozice(new Pozice($pozice, 'RD'));
                if ($hokejista->id != 0) {
                    $this->vylouceni->setVylouceni($hokejista, $trest->cas, $hosteDomaci);
                    $this->utkaniPrubeh->setCasRotaceVylouceni(0);
                }
            } else { //LD
                $hokejista = $sestava->getHracPozice(new Pozice($pozice, 'LD'));
                if ($hokejista->id != 0) {
                    $this->vylouceni->setVylouceni($hokejista, $trest->cas, $hosteDomaci);
                    $this->utkaniPrubeh->setCasRotaceVylouceni(0);
                }
            }

            $this->statistiky->setUdalost($hokejista->id, $trest->typudalost_id, $hosteDomaci);
        }
    }

    /**
     * Vybere nahodne delku vylouceni
     * @return int
     */
    private static function casTrest() {
        $nahoda = self::nahoda();

        //k jakemu dojde vylouceni
        if ($nahoda > self::PR_TREST_120 AND $nahoda <= self::PR_TREST_240) {
            return (object) array('cas' => self::TREST_120, 'typudalost_id' => 6);
        } elseif ($nahoda > self::PR_TREST_240 AND $nahoda <= self::PR_TREST_300) {
            return (object) array('cas' => self::TREST_240, 'typudalost_id' => 17);
        } else {
            return (object) array('cas' => self::TREST_300, 'typudalost_id' => 18);
        }
    }

    /**
     * Vyhodnoti se jestli se bude utocit a kdo
     */
    private function utocnaAkce() {
        //jestli probehne nejaky utok
        if (self::nahoda() <= self::PR_STRELA) {
            $sance = $this->sestavaD->getParametryUtok() / ($this->sestavaD->getParametryUtok() + $this->sestavaH->getParametryUtok());
            if ($sance > 0.85) {
                $sance = 0.85;
            } elseif ($sance < 0.15) {
                $sance = 0.15;
            }

            //kdo utoci?
            if (self::nahoda() <= $sance) { //utoci domaci
                $sestavaUtok = $this->sestavaD;
                $sestavaObrana = $this->sestavaH;
                $brankar = $this->tymHoste->getBrankarFormace();

                $strelaVsBlok = $this->strelaVsBlok($sestavaUtok, $sestavaObrana, 1);
                if ($strelaVsBlok['uspech']) { //jestli dojde ke strele
                    $gol = $this->strelaPozice($sestavaUtok, $brankar, 1, $strelaVsBlok['asistence']);
                    if ($gol) {
                        $this->utkaniPrubeh->setGolyDomaci();
                        $this->vylouceni->golVylouceni(0);
                        $this->nastavPlusMinus(1);
                    }
                }
            } else { //utoci hoste               
                $sestavaObrana = $this->sestavaD;
                $sestavaUtok = $this->sestavaH;
                $brankar = $this->tymTomaci->getBrankarFormace();
                $strelaVsBlok = $this->strelaVsBlok($sestavaUtok, $sestavaObrana, 0);
                if ($strelaVsBlok['uspech']) { //jestli dojde ke strele
                    $gol = $this->strelaPozice($sestavaUtok, $brankar, 0, $strelaVsBlok['asistence']);
                    if ($gol) {
                        $this->utkaniPrubeh->setGolyHoste();
                        $this->vylouceni->golVylouceni(1);
                        $this->nastavPlusMinus(0);
                    }
                }
            }
        }
    }

    /**
     * Nastavi plus minus hokejistum
     * @param bool $hosteDomaci 1-domaci
     */
    private function nastavPlusMinus($hosteDomaci) {
        foreach ($this->sestavaD->getSestava() AS $hokejista) {
            if ($hokejista->id) {
                $this->statistiky->setUdalost($hokejista->id, $hosteDomaci ? 9 : 10, 1);
            }
        }
        foreach ($this->sestavaH->getSestava() AS $hokejista) {
            if ($hokejista->id) {
                $this->statistiky->setUdalost($hokejista->id, $hosteDomaci ? 10 : 9, 0);
            }
        }
    }

    /**
     * Je pusten tym ke strele a prosel do utocneho pasma?
     * @param Formace $sestavaUtok
     * @param Formace $sestavaObrana
     * @param bool $domaciHoste D-1, H-0
     * @return array
     */
    private function strelaVsBlok($sestavaUtok, $sestavaObrana, $domaciHoste) {

        $sance = $sestavaUtok->getParametryStrelaBlokUtok() / ($sestavaUtok->getParametryStrelaBlokUtok() + $sestavaObrana->getParametryStrelaBlokObrana());
 
        //asistence
        $hokejistaAsistence = array();
        if (self::nahoda() <= 0.9) {
            $rand1 = rand(1, 5);
            $rand2 = rand(1, 5);
            $hokejista1 = $sestavaUtok->getHracPozice(new Pozice('zakladni', $rand1));
            if ($hokejista1->id != 0) { //asistence nemuze byt vylouceny hrac
                $hokejistaAsistence[] = $hokejista1;
            }
            if ($rand1 != $rand2) {
                $hokejista2 = $sestavaUtok->getHracPozice(new Pozice('zakladni', $rand2));
                if ($hokejista2->id != 0) {
                    $hokejistaAsistence[] = $hokejista2;
                }
            }
        }

        if (self::nahoda() <= $sance) {
            return array('uspech' => TRUE, 'asistence' => $hokejistaAsistence);
        } else {
            return array('uspech' => 0, 'asistence' => array());
        }
    }

    /**
     * Vyhodnoti se kdo strili na branku
     * @param Formace $sestavaUtok
     * @param BrankarFormace $brankar
     * @param bool $utok utcoci 1-domaci / 0-hoste
     * @param array $asistence Pole Hokejista s asistencemi
     * @return bool Padl gol ANO/NE
     */
    private function strelaPozice($sestavaUtok, $brankar, $utok, $asistence) {
        $sanceRW = $sestavaUtok->getParametrPoziceStrela(new Pozice('zakladni', 'RW'));
        $sanceC = $sestavaUtok->getParametrPoziceStrela(new Pozice('zakladni', 'C'));
        $sanceLW = $sestavaUtok->getParametrPoziceStrela(new Pozice('zakladni', 'LW'));
        $sanceRD = $sestavaUtok->getParametrPoziceStrela(new Pozice('zakladni', 'RD'));
        $sanceLD = $sestavaUtok->getParametrPoziceStrela(new Pozice('zakladni', 'LD'));

        $nahoda = self::nahoda();
        //Jaka pozice strili
        if ($nahoda <= ($sanceRW / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD))) { //strili RW
            return $this->strela($sestavaUtok, $brankar, new Pozice('zakladni', 'RW'), $utok, $asistence);
        } elseif ($nahoda > ($sanceRW / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD)) AND $nahoda <= (($sanceRW + $sanceC) / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD))) { //strili C
            return $this->strela($sestavaUtok, $brankar, new Pozice('zakladni', 'C'), $utok, $asistence);
        } elseif ($nahoda > (($sanceRW + $sanceC) / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD)) AND $nahoda <= (($sanceRW + $sanceC + $sanceLW) / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD))) { //strili LW
            return $this->strela($sestavaUtok, $brankar, new Pozice('zakladni', 'LW'), $utok, $asistence);
        } elseif ($nahoda > (($sanceRW + $sanceC + $sanceLW) / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD)) AND $nahoda <= (($sanceRW + $sanceC + $sanceLW + $sanceRD) / ($sanceRW + $sanceC + $sanceLW + $sanceRD + $sanceLD))) { //strili RD
            return $this->strela($sestavaUtok, $brankar, new Pozice('zakladni', 'RD'), $utok, $asistence);
        } else { //strili LD
            return $this->strela($sestavaUtok, $brankar, new Pozice('zakladni', 'LD'), $utok, $asistence);
        }
    }

    /**
     * Strela na branku konkretniho hrace proti brankari
     * @param Formace $sestavaUtok
     * @param BrankarFormace $brankar
     * @param \Model\Calculation\Match\Pozice $pozice
     * @param bool $utok utcoci 1-domaci / 0-hoste
     * @param array $asistence Pole Hokejista s asistencemi
     * @return bool Padl gol ANO/NE
     */
    private function strela($sestavaUtok, $brankar, Pozice $pozice, $utok, $asistence) {

        //kdo utoci tomu zjistime jakeho ma aktualne brankare
        $tretina = $this->utkaniPrubeh->getTretina();
        if ($utok) {
            $poziceBrankar = new Pozice('brankar', $this->tymHoste->getAktualiPoziceBrankar()); //bude se menit pri stridani
        } else {
            $poziceBrankar = new Pozice('brankar', $this->tymTomaci->getAktualiPoziceBrankar());
        }

        $sance = $sestavaUtok->getParametryStrelaUtok($pozice) / ($sestavaUtok->getParametryStrelaUtok($pozice) + $brankar->getParametryStrelaBrana($poziceBrankar));

        if ($sance > 0.3) {
            $sance = 0.3;
        } elseif ($sance < 0.05) {
            $sance = 0.05;
        }

        //strela na branku
        if (self::nahoda() <= $sance) { // GOÓOL
            $this->statistiky->setUdalost($sestavaUtok->getHracPozice($pozice)->id, $this->golTypudalost($utok), $utok);
            $this->statistiky->setUdalost($brankar->getHracPozice($poziceBrankar)->id, 21, $utok ? 0 : 1);
            // asistence
            $i = 1;
            foreach ($asistence AS $hoejista) {
                if ($hoejista->id != $sestavaUtok->getHracPozice($pozice)->id) {
                    $this->statistiky->setUdalost($hoejista->id, $i == 1 ? 4 : 19, $utok);
                    $i++;
                }
            }
            return 1;
        } else { // strela            
            $this->statistiky->setUdalost($sestavaUtok->getHracPozice($pozice)->id, 5, $utok);
            $this->statistiky->setUdalost($brankar->getHracPozice($poziceBrankar)->id, 20, $utok ? 0 : 1);

            return 0;
        }
    }

    /**
     * Vrati typudalost ID pokud padl gol
     * @param bool $utok
     * @return int
     */
    private function golTypudalost($utok) {
        $pocetVylouceniD = count($this->vylouceni->isVylouceni()->domaci);
        $pocetVylouceniH = count($this->vylouceni->isVylouceni()->hoste);

        if ($pocetVylouceniD == 0 AND $pocetVylouceniH == 1) { // 5:4
            return $utok ? 2 : 3;
        } elseif ($pocetVylouceniD == 1 AND $pocetVylouceniH == 0) { //4:5       
            return $utok ? 3 : 2;
        } elseif ($pocetVylouceniD == 1 AND $pocetVylouceniH == 1) { //4:4       
            return 15;
        } elseif ($pocetVylouceniD == 0 AND $pocetVylouceniH == 2) { //5:3       
            return $utok ? 11 : 13;
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 0) { //3:5       
            return $utok ? 13 : 11;
        } elseif ($pocetVylouceniD == 1 AND $pocetVylouceniH == 2) { //4:3       
            return $utok ? 12 : 14;
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 1) { //3:4       
            return $utok ? 14 : 13;
        } elseif ($pocetVylouceniD == 2 AND $pocetVylouceniH == 2) { //3:3       
            return 15;
        } else {
            return 1;
        }
    }

    /**
     * Samostatne najzedy
     */
    private function samostatneNajezdy() {
        $golyDomaci = 0;
        $golyHoste = 0;
        $cas = 4320;
        $pocetNajezdu = 1;
        $i = 1;

        $brankarD = $this->tymTomaci->getBrankarFormace();
        $brankarH = $this->tymHoste->getBrankarFormace();
        $najezdyD = $this->tymTomaci->getNajezdyFormace();
        $najezdyH = $this->tymHoste->getNajezdyFormace();

        //kdo utoci tomu zjistime jakeho ma aktualne brankare
        $poziceBrankarD = new Pozice('brankar', $this->tymTomaci->getAktualiPoziceBrankar());
        $poziceBrankarH = new Pozice('brankar', $this->tymHoste->getAktualiPoziceBrankar());

        do { //jedou se najezdy
            $najezdPozice = new Pozice('najezdy', $i);

            //domaci            
            $sance = $najezdyD->getParametryNajezdStrela($najezdPozice) / ($najezdyD->getParametryNajezdStrela($najezdPozice) + $brankarH->getParametryNajezdBrana($poziceBrankarH));
            $this->statistiky->setUdalost($najezdyD->getHracPozice($najezdPozice)->id, 5, 1);
            if (self::nahoda() <= $sance) {
                $golyDomaci++;
                $this->statistiky->setUdalost($najezdyD->getHracPozice($najezdPozice)->id, 1, 1);
            }

            //hoste            
            $sance = $najezdyH->getParametryNajezdStrela($najezdPozice) / ($najezdyH->getParametryNajezdStrela($najezdPozice) + $brankarD->getParametryNajezdBrana($poziceBrankarD));
            $this->statistiky->setUdalost($najezdyH->getHracPozice($najezdPozice)->id, 5, 0);
            if (self::nahoda() <= $sance) {
                $golyHoste++;
                $this->statistiky->setUdalost($najezdyH->getHracPozice($najezdPozice)->id, 1, 0);
            }

            if ($pocetNajezdu % 3 == 0) { //treti pruchod vynulujeme pozici hrace na najezd
                $cas += 60;
                $i = 0;
            }

            $i++;
            $pocetNajezdu++;
        } while (($golyDomaci == $golyHoste) OR ($pocetNajezdu < 4));

        if ($golyDomaci > $golyHoste) {
            $this->utkaniPrubeh->setGolyDomaci();
        } else {
            $this->utkaniPrubeh->setGolyHoste();
        }
    }

    /**
     * Po zapase se spusti update energie a zkusenosti
     */
    private function updateHokejistaPoZapase() {
        $this->tymTomaci->zapasEnergieZkusenosti($this->statistiky, 1);
        $this->tymHoste->zapasEnergieZkusenosti($this->statistiky, 0);
    }

    /**
     * Zjisti a ulozi do udalostsestava jak hral tym v jednotlivych tretinach
     */
    private function statistkyPoTretinach() {
        foreach ($this->utkaniPrubeh->getTriFormace() AS $DH => $tretina) {
            if ($DH == 'domaci') {
                $domaciHoste = 1;
            } else {
                $domaciHoste = 0;
            }
            foreach ($tretina AS $poradiTretina => $hodnotaTretina) {
                $this->statistiky->setUdalostsestava(NULL, $poradiTretina + 199, $domaciHoste, $hodnotaTretina);
            }
        }
        foreach ($this->utkaniPrubeh->getBrankarPozice() AS $DH => $tretina) {
            if ($DH == 'domaci') {
                $domaciHoste = 1;
            } else {
                $domaciHoste = 0;
            }
            foreach ($tretina AS $poradiTretina => $hodnotaTretina) {
                $this->statistiky->setUdalostsestava(NULL, $poradiTretina + 204, $domaciHoste, $hodnotaTretina);
            }
        }
    }

    /**
     * Pripravi sestavu pro ulozeni jak byl odehran zapas
     */
    private function nastavUdalostsestava() {
        $sestavaD = $this->tymTomaci->pripravUdalostsestava();
        foreach ($sestavaD AS $typudalost_id => $hokejista_id) {
            $this->statistiky->setUdalostsestava($hokejista_id, $typudalost_id, 1);
        }
        $sestavaH = $this->tymHoste->pripravUdalostsestava();
        foreach ($sestavaH AS $typudalost_id => $hokejista_id) {
            $this->statistiky->setUdalostsestava($hokejista_id, $typudalost_id, 0);
        }
    }

    /**
     * Zraneni hokejistu maximale 2 v zapase
     */
    public function zraneniHokejistu() {
        $zraneni = array();

        //vyberou se hraci potencialne zraneni
        for ($i = 1; $i <= 4; $i++) {
            foreach ($this->tymTomaci->getZakladniFormace($i)->getSestava() AS $hokejista) {
                if ($hokejista->id != 0) {
                    $pocetDni = rand(1, 4);
                    $koeficient = 0.001;
                    if (($hokejista->potencial - $hokejista->vek) < 0) { //pokud vek presahne potencial zraneni je uz pravdepodobne kazdym rokem o 4%
                        $koeficient = 0.04 * ($hokejista->potencial - $hokejista->vek);
                        $pocetDni = ($hokejista->potencial - $hokejista->vek) + $pocetDni;
                    }

                    //zohledneni energie
                    $koeficient += (100 - $hokejista->energie) * 0.0001;

                    if (self::nahoda() <= $koeficient) {
                        $zraneni[] = array('hokejista' => $hokejista, 'domaciHoste' => 1, 'pocetDni' => $pocetDni);
                    }
                }
            }
            foreach ($this->tymHoste->getZakladniFormace($i)->getSestava() AS $hokejista) {
                if ($hokejista->id != 0) {
                    $koeficient = 0.001;
                    $pocetDni = rand(1, 4);
                    if (($hokejista->potencial - $hokejista->vek) < 0) { //pokud vek presahne potencial zraneni je uz pravdepodobne kazdym rokem o 4%
                        $koeficient = 0.04 * ($hokejista->vek - $hokejista->potencial );
                        $pocetDni = ($hokejista->potencial - $hokejista->vek) + $pocetDni;
                    }

                    //zohledneni energie
                    $koeficient += (100 - $hokejista->energie) * 0.0001;

                    if (self::nahoda() <= $koeficient) {
                        $zraneni[] = array('hokejista' => $hokejista, 'domaciHoste' => 0, 'pocetDni' => $pocetDni);
                    }
                }
            }
        }

        //z potencionalne zranenych se vyberou max 2, kteri se zrani v zapase
        $pocet = count($zraneni);

        if ($pocet == 1) {
            $this->statistiky->setUdalost($zraneni[0]['hokejista']->id, 8, $zraneni[0]['domaciHoste']);
            $zraneni[0]['hokejista']->updateHokejistaZraneni($zraneni[0]['pocetDni']);
            $this->zraneni[] = array('hokejista_id' => $zraneni[0]['hokejista']->id, 'tym_id' => $zraneni[0]['domaciHoste'] ? $this->tymTomaci->id : $this->tymHoste->id);
        } elseif ($pocet == 2) {
            $this->statistiky->setUdalost($zraneni[0]['hokejista']->id, 8, $zraneni[0]['domaciHoste']);
            $zraneni[0]['hokejista']->updateHokejistaZraneni($zraneni[0]['pocetDni']);
            $this->zraneni[] = array('hokejista_id' => $zraneni[0]['hokejista']->id, 'tym_id' => $zraneni[0]['domaciHoste'] ? $this->tymTomaci->id : $this->tymHoste->id);
            $this->statistiky->setUdalost($zraneni[1]['hokejista']->id, 8, $zraneni[1]['domaciHoste']);
            $zraneni[1]['hokejista']->updateHokejistaZraneni($zraneni[1]['pocetDni']);
            $this->zraneni[] = array('hokejista_id' => $zraneni[1]['hokejista']->id, 'tym_id' => $zraneni[1]['domaciHoste'] ? $this->tymTomaci->id : $this->tymHoste->id);
        } elseif ($pocet > 2) {
            $vylouceni1 = rand(0, $pocet - 1);
            $vylouceni2 = rand(0, $pocet - 1);

            if ($vylouceni1 == $vylouceni2) {
                $this->statistiky->setUdalost($zraneni[$vylouceni1]['hokejista']->id, 8, $zraneni[$vylouceni1]['domaciHoste']);
                $zraneni[$vylouceni1]['hokejista']->updateHokejistaZraneni($zraneni[$vylouceni1]['pocetDni']);
                $this->zraneni[] = array('hokejista_id' => $zraneni[$vylouceni1]['hokejista']->id, 'tym_id' => $zraneni[$vylouceni1]['domaciHoste'] ? $this->tymTomaci->id : $this->tymHoste->id);
            } else {
                $this->statistiky->setUdalost($zraneni[$vylouceni1]['hokejista']->id, 8, $zraneni[$vylouceni1]['domaciHoste']);
                $zraneni[$vylouceni1]['hokejista']->updateHokejistaZraneni($zraneni[$vylouceni1]['pocetDni']);
                $this->zraneni[] = array('hokejista_id' => $zraneni[$vylouceni1]['hokejista']->id, 'tym_id' => $zraneni[$vylouceni1]['domaciHoste'] ? $this->tymTomaci->id : $this->tymHoste->id);
                $this->statistiky->setUdalost($zraneni[$vylouceni2]['hokejista']->id, 8, $zraneni[$vylouceni2]['domaciHoste']);
                $zraneni[$vylouceni2]['hokejista']->updateHokejistaZraneni($zraneni[$vylouceni2]['pocetDni']);
                $this->zraneni[] = array('hokejista_id' => $zraneni[$vylouceni2]['hokejista']->id, 'tym_id' => $zraneni[$vylouceni2]['domaciHoste'] ? $this->tymTomaci->id : $this->tymHoste->id);
            }
        }
    }

    /**
     * Vrati zraneni ze zapasu
     * @return array
     */
    public function getZraneni() {
        return $this->zraneni;
    }

    /**
     * Ulozi vysledky zapasu
     */
    private function ulozVysledekZapasu() {
        $priznak = NULL;
        if ($this->utkaniPrubeh->getProdlouzeni()) {
            $priznak = 'OT';
        }

        if ($this->utkaniPrubeh->getProdlouzeni() AND $this->utkaniPrubeh->getNajezdy()) {
            $priznak = 'SO';
        }
        
        if (!DEBUG) {
            $this->utkani->update(array(
                'golyDomaci' => $this->utkaniPrubeh->getGolyDomaci(),
                'golyHoste' => $this->utkaniPrubeh->getGolyHoste(),
                'priznak' => $priznak
            ));
        }
    }

    /**
     * Pole se statistikma golmanu
     * @return array
     */
    public function getStatisikyGolmani() {
        return $this->statistiky->getStatisikyGolmani();
    }

    /**
     * @return UtkaniPrubeh
     */
    public function getUtkaniPrubeh() {
        return $this->utkaniPrubeh;
    }

    /**
     * @return array
     */
    public function getUdalost() {
        return $this->statistiky->getUdalost();
    }

    /**
     * @return array
     */
    public function getUdalostsestava() {
        return $this->statistiky->getUdalostsestava();
    }

    /**
     * Vygeneruje nahodne clislo mezi 0-1
     * @return float
     */
    private static function nahoda() {
        return mt_rand() / mt_getrandmax();
    }

    // ---UKOLY---
}
