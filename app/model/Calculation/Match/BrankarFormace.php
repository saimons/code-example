<?php

namespace Model\Calculation\Match;

class BrankarFormace extends Formace {

    /**
     * @param int $formace
     * @param \Model\Calculation\Match\Taktika $taktika
     */
    public function __construct($formace, Taktika $taktika = NULL) {
        parent::__construct($formace, $taktika);
    }

    /**
     * Parametr pro strelu na branku
     * @param \Model\Calculation\Match\Pozice $pozice
     * @return float
     */
    public function getParametryStrelaBrana(Pozice $pozice) {
        $hokejista = $this->getHracPozice($pozice);
        return self::KO_BRANA_GLOBAL * ($hokejista->brana + $hokejista->sila * self::KO_BRANA_SILA + $hokejista->zkusenosti * self::KO_BRANA_ZKUSENOSTI + $hokejista->sebeovladani * self::KO_BRANA_SEBEOVLADANI);
    }
    
        /**
     * 
     * @param \Model\Calculation\Match\Pozice $pozice
     * @return int
     */
    public function getParametryNajezdBrana(Pozice $pozice) {
        $hokejista = $this->getHracPozice($pozice);
        return $hokejista->brana + self::KO_SN_BRANA_SILA * $hokejista->sila + self::KO_SN_BRANA_SEBEOVLADANI * $hokejista->sebeovladani + self::KO_SN_BRANA_ZKUSENOSTI * $hokejista->zkusenosti;
    }

    /**
     * Updejtuje na konci zapasu energie a zkusenosti
     * @param float $koeficient
     */
    public function setZapasEnergieZkusenosti($statistiky, Pozice $pozice, $koeficient, $koeficientTyputkani) {
        $hokejista = $this->getHracPozice($pozice);
        $hokejista->updateHokejistaPoZapase($statistiky, $koeficient, $koeficientTyputkani);
    }
}
