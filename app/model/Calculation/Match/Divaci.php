<?php

namespace Model\Calculation\Match;

use Nette\Object;

/**
 * Model pro vypocet navstevnosti
 */
class Divaci extends Object {

    /**
     * @var \Nette\Database\Table\Selection
     */
    private $utkani;

    /**
     * @var int
     */
    private $pocetDivaku;

    /**
     * @var int
     */
    private $vstupne;

    /**
     * @var int
     */
    private $penizeTomaci;

    /**
     * @var int
     */
    private $penizeHoste;

    /**
     * @var Tym
     */
    private $tymTomaci;

    /**
     * @var Tym
     */
    private $tymHoste;

    /**
     * @param \Nette\Database\Table\Selection $utkani
     * @param Tym $tymTomaci
     * @param Tym $tymHoste
     */
    public function __construct($utkani, Tym $tymTomaci, Tym $tymHoste) {
        $this->utkani = $utkani;
        $this->tymTomaci = $tymTomaci;
        $this->tymHoste = $tymHoste;
    }

    /**
     * @return int
     */
    public function getPocetDivaku() {
        return (int) $this->pocetDivaku;
    }
    
    /**
     * Vstupne zapasu
     * @return int
     */
    public function getVstupne() {
        return (int) $this->vstupne;
    }

    /**
     * @return int
     */
    public function getPenizeTomaci() {
        return (int) floor($this->penizeTomaci);
    }

    /**
     * @return int
     */
    public function getPenizeHoste() {
        return (int) floor($this->penizeHoste);
    }

    /**
     * Zavola vypocet jednoho utkani
     * @param \Nette\Database\Table\Selection $utkani
     */
    public function run() {
        $this->vypoctiVstupne();
        $this->zjistiPocetDivaku();
        $this->rozdelPenize();
    }

    /**
     * Zjisti vstupne na zapas
     */
    private function vypoctiVstupne() {
        if ($this->utkani->typutkani_id == 100) { //pratelak
            $this->vstupne = $this->tymTomaci->getVstupnePratelsky();
        } else { //liga
            $this->vstupne = $this->tymTomaci->getVstupneLiga();
        }

        // uprava ceny vstupneho tak, aby byla v rozmezi 0 - 1500
        if ($this->vstupne < 0) {
            $this->vstupne = 0;
        } elseif ($this->vstupne > 1500) {
            $this->vstupne = 1500;
        }
    }

    private function zjistiPocetDivaku() {
        if ($this->vstupne < 700) {
            $this->pocetDivaku = 6000 + 8 * (1 - pow(1.002, (700 - $this->vstupne)) / (1 - 1.002));
        } elseif ($this->vstupne == 700) {
            $this->pocetDivaku = 6000;
        } elseif ($this->vstupne > 700) {
            $this->pocetDivaku = 6000 - 4 * ((1 - pow(1.0004, ($this->vstupne - 700))) / (1 - 1.0004));
        }

        // uprava o nahodny faktor v navstevnosti
        $this->pocetDivaku *= ((0.93 + ((mt_rand() / mt_getrandmax()) * 0.12)));
        $this->bonusyZaTypUtkani();
                
        if ($this->pocetDivaku > $this->tymTomaci->getKapacitaStadion()) {
            $this->pocetDivaku = $this->tymTomaci->getKapacitaStadion();
        }       
        $this->pocetDivaku = round($this->pocetDivaku);
    }

    /**
     * update divaciZapas podle ligy - prvni tri ligy maji bonus
     */
    private function bonusyZaTypUtkani() {
        switch ($this->zjistiNejnizsiliga()) {
            case 1:
                $this->pocetDivaku += 5000;
                break;
            case 2:
                $this->pocetDivaku += 4000;
                break;
            case 3:
                $this->pocetDivaku += 3000;
                break;
            case 4:
                $this->pocetDivaku += 2000;
                break;
            case 5:
                $this->pocetDivaku += 1000;
                break;
        }

        switch ($this->utkani->typutkani_id) {
            case 100:
                $this->pocetDivaku *= 0.6;
                break;
            case 60:
            case 61:
            case 62:
                $this->pocetDivaku *= 1.1;
                break;
            case 63:
            case 64:
            case 65:
                $this->pocetDivaku *= 1.2;
                break;
            case 66:
            case 67:
            case 68:
                $this->pocetDivaku *= 1.3;
                break;
        }
    }

    /**
     * Minimalni liga v konkretnim zapase
     * @return int
     */
    private function zjistiNejnizsiliga() {
        $poradiLigaDomaci = $this->tymTomaci->getPoradiLiga();
        $poradiLigaHoste = $this->tymHoste->getPoradiLiga();

        if ($poradiLigaDomaci < $poradiLigaHoste) {
            return $poradiLigaDomaci;
        } elseif ($poradiLigaHoste < $poradiLigaDomaci) {
            return $poradiLigaHoste;
        } else {
            return $poradiLigaDomaci;
        }
    }

    /**
     * Rozdeli penize podle zapasu
     */
    private function rozdelPenize() {
        if ($this->utkani->typutkani_id != 100) {
            $this->penizeTomaci = $this->pocetDivaku * $this->vstupne;
        } else {
            $this->penizeTomaci = ($this->pocetDivaku * $this->vstupne * 2) / 3;
            $this->penizeHoste = ($this->pocetDivaku * $this->vstupne) / 3;
        }
    }

}
