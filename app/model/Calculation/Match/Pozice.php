<?php

namespace Model\Calculation\Match;

use Nette\Object;

/**
 * Model pro vypocet zapasu
 */
class Pozice extends Object {

    /**
     * Pozice v poli pro hrace     
     * @var string
     */
    private $pozice;
    
    /**
     * 
     * @param string $typ
     * @param int $pozice 1-RW,2-C,3-LW,4-RD,5-LD
     */
    public function __construct($typ, $pozice) {
        $this->nastavPozici($typ, $pozice);        
    }
    
    /**
     * @return string
     */
    public function getPozice() {
        return $this->pozice;
    }

        /**
     * Podle typu formace vybere spravne rozlozeni
     * @param string $typ
     * @param int $pozice
     */
    private function nastavPozici($typ, $pozice) {
        switch ($typ) {
            case 'brankar':
                $this->pozice = $pozice;
            case 'zakladni':
                $this->pozice = $this->zakladniFormace($pozice);
                break;
            case 'presilovka':
                $this->pozice = $this->zakladniFormace($pozice);
                break;
            case 'oslabeni':
                $this->pozice = $this->oslabeniFormace($pozice);
                break;
            case 'najezdy':
                $this->pozice = $pozice;
                break;
                
        }
    } 
    
    /**
     * Rozdeleni pro zakladni sestavu
     * @param int $pozice
     * @return string
     */
    private function zakladniFormace($pozice) {        
        switch ($pozice) {
            case 1:
            case 'RW':
                return 'RW';
            case 2:
            case 'C':    
                return 'C';
            case 3:
            case 'LW':
                return 'LW';
            case 4:
            case 'RD':
                return 'RD';
            case 5:
            case 'LD':
                return 'LD';                 
        }        
    }
    
    /**
     * Rozdeleni pro oslabeni sestavu
     * @param int $pozice
     * @return string
     */
    private function oslabeniFormace($pozice) {
        switch ($pozice) {
            case 1:
                return 'RW';
            case 2:
                return 'C';
            case 4:
                return 'RD';
            case 5:
                return 'LD';                 
        }
    }


}
