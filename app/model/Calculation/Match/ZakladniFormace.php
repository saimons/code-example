<?php

namespace Model\Calculation\Match;

class ZakladniFormace extends Formace {

    /** 
     * @param int $formace
     * @param \Model\Calculation\Match\Taktika $taktika
     */
    public function __construct($formace, Taktika $taktika) {
        parent::__construct($formace, $taktika);
    }
    
    /**
     * Updejtuje na konci zapasu energie a zkusenosti
     * @param float $koeficient
     */
    public function setZapasEnergieZkusenosti($statistiky, $koeficient, $koeficientTyputkani) {
        foreach ($this->sestava AS $hokejista) {
            $hokejista->updateHokejistaPoZapase($statistiky, $koeficient, $koeficientTyputkani, $this->taktika->getCasLed($this->formace));
        }
    }
    
    /**
     * Vrati nastaveni taktik pro formaci
     * @return Taktika
     */
    public function getCasLed() {
        return $this->taktika->getCasLed($this->formace);
    }
    
    /**
     * Zjisti se jestli se utok povedl nebo se ho podarilo zablokovat, parametry pro utocici tym
     * @return float
     */
    public function getParametryStrelaBlokUtok() {
        $parametr = 0;
        foreach ($this->sestava AS $pozice => $hokejista) {
            switch ($pozice) {
                case 'RW':
                case 'C':
                case 'LW':
                    $parametr += 
                        $hokejista->utok * $this->taktika->getTaktikaUtok($this->formace)->utocnik_utok * $this->taktika->getUtokObranaUtok($this->formace) + 
                        $hokejista->obrana * $this->taktika->getTaktikaUtok($this->formace)->utocnik_obrana * $this->taktika->getUtokObranaObrana($this->formace) + 
                        $hokejista->sila * $this->taktika->getTaktikaUtok($this->formace)->utocnik_sila + 
                        $hokejista->strela * $this->taktika->getTaktikaUtok($this->formace)->utocnik_strela + 
                        $hokejista->nahravka * $this->taktika->getTaktikaUtok($this->formace)->utocnik_nahravka + 
                        $hokejista->sebeovladani * $this->taktika->getTaktikaUtok($this->formace)->utocnik_sebeovladani + 
                        $hokejista->rychlost * $this->taktika->getTaktikaUtok($this->formace)->utocnik_rychlost + 
                        $hokejista->brana * $this->taktika->getTaktikaUtok($this->formace)->brana + 
                        $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI;
                    break;

                case 'RD':
                case 'LD':
                    $hokejista->utok * $this->taktika->getTaktikaUtok($this->formace)->obrance_utok * $this->taktika->getUtokObranaUtok($this->formace) + 
                        $hokejista->obrana * $this->taktika->getTaktikaUtok($this->formace)->obrance_obrana * $this->taktika->getUtokObranaObrana($this->formace) + 
                        $hokejista->sila * $this->taktika->getTaktikaUtok($this->formace)->obrance_sila + 
                        $hokejista->strela * $this->taktika->getTaktikaUtok($this->formace)->obrance_strela + 
                        $hokejista->nahravka * $this->taktika->getTaktikaUtok($this->formace)->obrance_nahravka + 
                        $hokejista->sebeovladani * $this->taktika->getTaktikaUtok($this->formace)->obrance_sebeovladani + 
                        $hokejista->rychlost * $this->taktika->getTaktikaUtok($this->formace)->obrance_rychlost + 
                        $hokejista->brana * $this->taktika->getTaktikaUtok($this->formace)->brana + 
                        $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI;
                    break;
            }            
        }
        return $parametr;
    }

    /**
     * Zjisti se jestli se utok povedl nebo se ho podarilo zablokovat, parametry pro branici tym
     * @return float
     */
    public function getParametryStrelaBlokObrana() {
        $parametr = 0;
        foreach ($this->sestava AS $pozice => $hokejista) {
            switch ($pozice) {
                case 'RW':
                case 'C':
                case 'LW':
                    $parametr += 
                        $hokejista->utok * $this->taktika->getTaktikaObrana($this->formace)->utocnik_utok * $this->taktika->getUtokObranaUtok($this->formace) + 
                        $hokejista->obrana * $this->taktika->getTaktikaObrana($this->formace)->utocnik_obrana * $this->taktika->getUtokObranaObrana($this->formace) + 
                        $hokejista->sila * $this->taktika->getTaktikaObrana($this->formace)->utocnik_sila + 
                        $hokejista->strela * $this->taktika->getTaktikaObrana($this->formace)->utocnik_strela + 
                        $hokejista->nahravka * $this->taktika->getTaktikaObrana($this->formace)->utocnik_nahravka + 
                        $hokejista->sebeovladani * $this->taktika->getTaktikaObrana($this->formace)->utocnik_sebeovladani + 
                        $hokejista->rychlost * $this->taktika->getTaktikaObrana($this->formace)->utocnik_rychlost + 
                        $hokejista->brana * $this->taktika->getTaktikaObrana($this->formace)->brana + 
                        $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI;
                    break;

                case 'RD':
                case 'LD':
                    $hokejista->utok * $this->taktika->getTaktikaObrana($this->formace)->obrance_utok * $this->taktika->getUtokObranaUtok($this->formace) + 
                        $hokejista->obrana * $this->taktika->getTaktikaObrana($this->formace)->obrance_obrana * $this->taktika->getUtokObranaObrana($this->formace) + 
                        $hokejista->sila * $this->taktika->getTaktikaObrana($this->formace)->obrance_sila + 
                        $hokejista->strela * $this->taktika->getTaktikaObrana($this->formace)->obrance_strela + 
                        $hokejista->nahravka * $this->taktika->getTaktikaObrana($this->formace)->obrance_nahravka + 
                        $hokejista->sebeovladani * $this->taktika->getTaktikaObrana($this->formace)->obrance_sebeovladani + 
                        $hokejista->rychlost * $this->taktika->getTaktikaObrana($this->formace)->obrance_rychlost + 
                        $hokejista->brana * $this->taktika->getTaktikaObrana($this->formace)->brana + 
                        $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI;
                    break;
            }
        }      
        return $parametr;
    }
    
    /**
     * Vrati sanci na utok pro porovnani utok D/H pokud se hraje 5:5
     * @return float
     */
    public function getParametryUtok() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->obrana * $this->taktika->getUtokObranaObrana($this->formace) + $hokejista->sila * self::KO_UTOK_SILA + $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI + $hokejista->utok * $this->taktika->getUtokObranaUtok($this->formace) + $hokejista->nahravka * self::KO_UTOK_NAHRAVKA;
        }
        return $parametr;
    }

}
