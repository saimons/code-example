<?php

namespace Model\Calculation\Match;

use Nette\Object;

class Tym extends Object {

    /**
     * @var \Nette\Database\Table\Selection
     */
    private $tym;

    /**
     * 1-domaci, 0-hoste
     * @var bool
     */
    private $domaciHoste;

    /**
     * @var int
     */
    private $vstupneLiga;

    /**
     * @var int
     */
    private $vstupnePratelsky;

    /**
     * @var int
     */
    private $poradiLiga;

    /**
     * @var int
     */
    private $poradiPodLiga;

    /**
     * @var int
     */
    private $kapacitaStadion;

    /**
     * @var \Nette\Database\Table\Selection
     */
    private $radarealna;

    /**
     * @var ZakladniFormace
     */
    private $zakladniFormace1;

    /**
     * @var ZakladniFormace
     */
    private $zakladniFormace2;

    /**
     * @var ZakladniFormace
     */
    private $zakladniFormace3;

    /**
     * @var ZakladniFormace
     */
    private $zakladniFormace4;

    /**
     * @var BrankarFormace
     */
    private $brankarFormace;

    /**
     * @var PresilovkaFormace
     */
    private $presilovkaFormace1;

    /**
     * @var PresilovkaFormace
     */
    private $presilovkaFormace2;

    /**
     * @var OslabeniFormace
     */
    private $oslabeniFormace1;

    /**
     * @var OslabeniFormace
     */
    private $oslabeniFormace2;

    /**
     * @var NajezdyFormace
     */
    private $najezdyFormace;

    /**
     * @var UtkaniPrubeh
     */
    private $utkaniPrubeh;

    /**
     * @var Nette\Database\Table\ActiveRow
     */
    private $aktualniSestava;

    /**
     *
     * @var Taktika
     */
    private $taktika;

    /**
     * 
     * @param \Nette\Database\Table\Selection $tym
     * @param bool $domaciHoste
     * @param \Nette\Database\Table\Selection $radarealna
     */
    public function __construct($tym, $domaciHoste, $radarealna) {
        $this->tym = $tym;
        $this->domaciHoste = $domaciHoste;
        $this->radarealna = $radarealna;
        $this->taktika = new Taktika($tym, $domaciHoste);
        $this->initTym();
    }
    
    /**    
     * @return int
     */
    public function getId() {
        return $this->tym->id;
    }

    /**
     * @return int
     */
    public function getVstupneLiga() {
        return $this->vstupneLiga;
    }

    /**
     * @return int
     */
    public function getVstupnePratelsky() {
        return $this->vstupnePratelsky;
    }

    /**
     * @return int
     */
    public function getPoradiLiga() {
        return $this->poradiLiga;
    }

    /**
     * @return int
     */
    public function getPoradiPodLiga() {
        return $this->poradiPodLiga;
    }

    /**
     * @return int
     */
    public function getKapacitaStadion() {
        return $this->kapacitaStadion;
    }
    
    /**     
     * @return Taktika
     */
    public function getTaktika() {
        return $this->taktika;
    }

    /**
     * @param \Model\Calculation\Match\UtkaniPrubeh $utkaniPrubeh
     */
    public function setUtkaniPrubeh(UtkaniPrubeh $utkaniPrubeh) {
        $this->utkaniPrubeh = $utkaniPrubeh;
        $this->taktika->setUtkaniPrubeh($this->utkaniPrubeh);
    }

    /**
     * @param int $cislo Ktera formace
     * @return ZakladniFormace
     */
    public function getZakladniFormace($cislo) {
        $promena = 'zakladniFormace' . $cislo;
        return $this->$promena;
    }

    /**
     * @return BrankarFormace
     */
    public function getBrankarFormace() {
        return $this->brankarFormace;
    }

    /**
     * @param int $cislo
     * @param bool $dvojita
     * @return PresilovkaFormace
     */
    public function getPresilovkaFormace($cislo) {
        $promena = 'presilovkaFormace' . $cislo;
        return $this->$promena;
    }

    /**
     * @param int $cislo
     * @param array $vylouceni
     * @param bool $dvojita
     * @return OslabeniFormace
     */
    public function getOslabeniFormace($cislo, $vylouceni, $dvojita = FALSE) {
        $promena = 'oslabeniFormace' . $cislo;
        $formaceOslabeni = $this->$promena;
        $formaceOslabeni->setDvojita($dvojita);
        $formaceOslabeni->setVylouceni($vylouceni);
        return $formaceOslabeni;
    }

    /**
     * @return NajezdyFormace
     */
    public function getNajezdyFormace() {
        return $this->najezdyFormace;
    }

    /**
     * @return int
     */
    public function getPocetLajn() {
        return $this->taktika->getPocetLajn();
    }

    /**
     * Inicializacni funkce na naplneni tridy
     */
    private function initTym() {
        $this->zjistiAktualniSestavu();
        $this->nastavPravidla();
        $this->zjistiVstupne();
        $this->zjistiLigu();
        $this->zjisitiVelikostStadion();
        $this->nastavZakladniFormace();
        $this->pocetLajn();
    }

    /**
     * Zjisti vstupne na konretni zapas (!!!)
     */
    private function zjistiVstupne() {
        $this->vstupneLiga = $this->tym->vstupneLiga;
        $this->vstupnePratelsky = $this->tym->vstupnePratelsky;
    }

    /**
     * Zjisti ligu ve ktere tym hraje
     */
    private function zjistiLigu() {
        $this->poradiLiga = $this->tym->liga->poradiLiga;
        $this->poradiPodLiga = $this->tym->liga->poradiPodLiga;
    }

    /**
     * Zjisti velikost stadionu
     */
    private function zjisitiVelikostStadion() {
        $this->kapacitaStadion = 0;
        foreach ($this->tym->related('stadion', 'tym_id') AS $s) {
            $sektor = $s->ref('stadionvar', 'stadionvarakt_id');
            $this->kapacitaStadion += ($sektor->kapacitaStani + $sektor->kapacitaSezeni);
        }
    }

    /**
     * Vytvoreni instanci pro jednotlive formace
     */
    private function nastavZakladniFormace() {
        foreach ($this->aktualniSestava->related('formace', 'sestava_id') AS $formace) {
            $nazev = 'zakladniFormace' . $formace->poradi;
            $this->$nazev = new ZakladniFormace($formace->poradi, $this->taktika);
            $this->taktika->setCasLed($formace->poradi, $formace->casLed);
            
            $taktikaUtok = $formace->ref('taktika', 'utok_taktika_id');
            $taktikaObrana = $formace->ref('taktika', 'obrana_taktika_id');
            
            $this->taktika->setTaktikaUtok($formace->poradi, $taktikaUtok); //parametry na utocnou taktiku
            $this->taktika->setTaktikaObrana($formace->poradi, $taktikaObrana); //prametry na obranou taktiku
            
            $this->taktika->setUtokObrana($formace->poradi, $formace->utocnaObranna); //hraju vic utocne nebo obrane
        }

        //nepredava se taktika + taktika pro oslabeni/presilovku bude jina
        $this->brankarFormace = new BrankarFormace(1);

        $this->presilovkaFormace1 = new PresilovkaFormace(1);
        $this->presilovkaFormace2 = new PresilovkaFormace(2);

        $this->oslabeniFormace1 = new OslabeniFormace(1);
        $this->oslabeniFormace2 = new OslabeniFormace(2);

        $this->najezdyFormace = new NajezdyFormace(1);
        $this->naplnFormace();
    }

    /**
     * Naplni formace hraci
     */
    private function naplnFormace() {
        $hokejista = array();
        foreach ($this->tym->related('hokejista', 'tym_id') AS $h) {
            $hokejista[$h->id] = new Hokejista($h);
        }

        foreach ($this->radarealna as $tym) {
            if ($tym->hokejista_id != 0) {
                switch ($tym->radaTyp) {
                    case '1G1':
                        $this->brankarFormace->setHokejista($hokejista[$tym->hokejista_id], new Pozice('brankar', 1));
                        break;
                    case '1G2':
                        $this->brankarFormace->setHokejista($hokejista[$tym->hokejista_id], new Pozice('brankar', 2));
                        break;
                    case '2F1':
                        $this->zakladniFormace1->setHokejista($hokejista[$tym->hokejista_id], new Pozice('zakladni', $tym->pozice));
                        break;
                    case '2F2':
                        $this->zakladniFormace2->setHokejista($hokejista[$tym->hokejista_id], new Pozice('zakladni', $tym->pozice));
                        break;
                    case '2F3':
                        $this->zakladniFormace3->setHokejista($hokejista[$tym->hokejista_id], new Pozice('zakladni', $tym->pozice));
                        break;
                    case '2F4':
                        $this->zakladniFormace4->setHokejista($hokejista[$tym->hokejista_id], new Pozice('zakladni', $tym->pozice));
                        break;
                    case '3P1':
                        $this->presilovkaFormace1->setHokejista($hokejista[$tym->hokejista_id], new Pozice('zakladni', $tym->pozice));
                        break;
                    case '3P2':
                        $this->presilovkaFormace2->setHokejista($hokejista[$tym->hokejista_id], new Pozice('zakladni', $tym->pozice));
                        break;
                    case '4O1':
                        $this->oslabeniFormace1->setHokejista($hokejista[$tym->hokejista_id], new Pozice('oslabeni', $tym->pozice));
                        break;
                    case '4O2':
                        $this->oslabeniFormace2->setHokejista($hokejista[$tym->hokejista_id], new Pozice('oslabeni', $tym->pozice));
                        break;
                    case '5N1':
                        $this->najezdyFormace->setHokejista($hokejista[$tym->hokejista_id], new Pozice('najezdy', 1));
                        break;
                    case '5N2':
                        $this->najezdyFormace->setHokejista($hokejista[$tym->hokejista_id], new Pozice('najezdy', 2));
                        break;
                    case '5N3':
                        $this->najezdyFormace->setHokejista($hokejista[$tym->hokejista_id], new Pozice('najezdy', 3));
                        break;
                }
            }
        }

        $nahradnikOslabeni = $this->zvolNahradnikOslabeni();
        $this->oslabeniFormace1->setNahradnikOslabeni($nahradnikOslabeni);
        $this->oslabeniFormace2->setNahradnikOslabeni($nahradnikOslabeni);
    }

    /**
     * Pripravi sestavu ve ktere odehrala zapas
     * @return array
     */
    public function pripravUdalostsestava() {
        $udalostsestava = array();       
        foreach ($this->radarealna as $tym) {
            switch ($tym->radaTyp) {
                case '1G1':
                    $udalostsestava[120] = $tym->hokejista_id;
                    break;
                case '1G2':
                    $udalostsestava[121] = $tym->hokejista_id;
                    break;
                case '2F1':
                    $typudalost_id = 99 + $tym->pozice;
                    $udalostsestava[$typudalost_id] = $tym->hokejista_id;
                    break;
                case '2F2':
                    $typudalost_id = 104 + $tym->pozice;
                    $udalostsestava[$typudalost_id] = $tym->hokejista_id;
                    break;
                case '2F3':
                    $typudalost_id = 109 + $tym->pozice;
                    $udalostsestava[$typudalost_id] = $tym->hokejista_id;
                    break;
                case '2F4':
                    $typudalost_id = 114 + $tym->pozice;
                    $udalostsestava[$typudalost_id] = $tym->hokejista_id;
                    break;
                case '3P1':
                    $typudalost_id = 121 + $tym->pozice;
                    $udalostsestava[$typudalost_id] = $tym->hokejista_id;
                    break;
                case '3P2':
                    $typudalost_id = 126 + $tym->pozice;
                    $udalostsestava[$typudalost_id] = $tym->hokejista_id;
                    break;
                case '4O1':
                    if ($tym->pozice == 1) {
                        $udalostsestava[132] = $tym->hokejista_id;
                    } elseif ($tym->pozice == 2) {
                        $udalostsestava[133] = $tym->hokejista_id;
                    } elseif ($tym->pozice == 4) {
                        $udalostsestava[134] = $tym->hokejista_id;
                    } elseif ($tym->pozice == 5) {
                        $udalostsestava[135] = $tym->hokejista_id;
                    }
                    break;
                case '4O2':
                    if ($tym->pozice == 1) {
                        $udalostsestava[136] = $tym->hokejista_id;
                    } elseif ($tym->pozice == 2) {
                        $udalostsestava[137] = $tym->hokejista_id;
                    } elseif ($tym->pozice == 4) {
                        $udalostsestava[138] = $tym->hokejista_id;
                    } elseif ($tym->pozice == 5) {
                        $udalostsestava[139] = $tym->hokejista_id;
                    }
                    break;
                case '5N1':
                    $udalostsestava[140] = $tym->hokejista_id;
                    break;
                case '5N2':
                    $udalostsestava[141] = $tym->hokejista_id;
                    break;
                case '5N3':
                    $udalostsestava[142] = $tym->hokejista_id;
                    break;
            }
        }
        return $udalostsestava;
    }

    /**
     * Vyberou se nahradnici na oslabeni, kteri nejsou 
     * @return array Nahradnik oslabeni
     */
    private function zvolNahradnikOslabeni() {
        $pouzitiHraci = array();

        foreach ($this->oslabeniFormace1->getSestava() AS $hokejista) {
            $pouzitiHraci[] = $hokejista->id;
        }

        foreach ($this->oslabeniFormace2->getSestava() AS $hokejista) {
            $pouzitiHraci[] = $hokejista->id;
        }
       

        $nahradnikOslabeny = array('W' => array(), 'D' => array());

        foreach ($this->zakladniFormace1->getSestava() AS $pozice => $hokejista) {
            if (!in_array($hokejista->id, $pouzitiHraci)) {
                switch ($pozice) {
                    case 'RW':
                    case 'LW':
                    case 'C':
                        count($nahradnikOslabeny['W']) < 2 ? $nahradnikOslabeny['W'][] = $hokejista : NULL;
                        break;

                    case 'RD':
                    case 'LD':
                        count($nahradnikOslabeny['D']) < 2 ? $nahradnikOslabeny['D'][] = $hokejista : NULL;
                        break;
                }
            }
        }

        foreach ($this->zakladniFormace2->getSestava() AS $pozice => $hokejista) {
            if (!in_array($hokejista->id, $pouzitiHraci)) {
                switch ($pozice) {
                    case 'RW':
                    case 'LW':
                    case 'C':
                        count($nahradnikOslabeny['W']) < 2 ? $nahradnikOslabeny['W'][] = $hokejista : NULL;
                        break;

                    case 'RD':
                    case 'LD':
                        count($nahradnikOslabeny['D']) < 2 ? $nahradnikOslabeny['D'][] = $hokejista : NULL;
                        break;
                }
            }
        }

        foreach ($this->zakladniFormace3->getSestava() AS $pozice => $hokejista) {
            if (!in_array($hokejista->id, $pouzitiHraci)) {
                switch ($pozice) {
                    case 'RW':
                    case 'LW':
                    case 'C':
                        count($nahradnikOslabeny['W']) < 2 ? $nahradnikOslabeny['W'][] = $hokejista : NULL;
                        break;

                    case 'RD':
                    case 'LD':
                        count($nahradnikOslabeny['D']) < 2 ? $nahradnikOslabeny['D'][] = $hokejista : NULL;
                        break;
                }
            }
        }

        foreach ($this->zakladniFormace4->getSestava() AS $pozice => $hokejista) {
            if (!in_array($hokejista->id, $pouzitiHraci)) {
                switch ($pozice) {
                    case 'RW':
                    case 'LW':
                    case 'C':
                        count($nahradnikOslabeny['W']) < 2 ? $nahradnikOslabeny['W'][] = $hokejista : NULL;
                        break;

                    case 'RD':
                    case 'LD':
                        count($nahradnikOslabeny['D']) < 2 ? $nahradnikOslabeny['D'][] = $hokejista : NULL;
                        break;
                }
            }
        }
        return $nahradnikOslabeny;
    }

    /**
     * 
     * @param int $tretina
     * @return int
     */
    public function getAktualiPoziceBrankar() {
        return $this->taktika->getAktualniPoziceBrankar();
    }

    /**
     * 
     * @param int $domaciHoste D-1,H-0
     */
    public function zapasEnergieZkusenosti($statistiky, $domaciHoste) {
        $triFormace = $this->utkaniPrubeh->getTriFormace();
        $brankarPozice = $this->utkaniPrubeh->getBrankarPozice();

        $koeficientTyputkani = 1;
        if ($this->utkaniPrubeh->getTyputkani_id() == 100) { // pratelak
            $koeficientTyputkani = 0.5;
        }

        $koeficientTretina = array(
            1 => 0.33, 2 => 0.33, 3 => 0.33, 4 => 0.1, 5 => 0.05
        );

        $koeficientFormace = array(
            'formace1' => 0, 'formace2' => 0, 'formace3' => 0, 'formace4' => 0, 'brankar1' => 0, 'brankar2' => 0
        );

        //iterujeme pres odehrane tretiny
        for ($i = 1; $i <= $this->utkaniPrubeh->getTretina(); $i++) {

            //spocita se koeficient pro formace
            $koeficientFormace['formace1'] += $koeficientTretina[$i];
            $koeficientFormace['formace2'] += $koeficientTretina[$i];
            $koeficientFormace['formace3'] += $koeficientTretina[$i];
            //\Tracy\Debugger::barDump($triFormace[$domaciHoste ? 'domaci' : 'hoste']);
            if ($triFormace[$domaciHoste ? 'domaci' : 'hoste'][$i] == 4) {
                $koeficientFormace['formace4'] += $koeficientTretina[$i];
            }            

            //spocita se koeficient pro brankare            
            if ($brankarPozice[$domaciHoste ? 'domaci' : 'hoste'][$i] == 1) {
                $koeficientFormace['brankar1'] += $koeficientTretina[$i];
            } elseif ($brankarPozice[$domaciHoste ? 'domaci' : 'hoste'][$i] == 2) {
                $koeficientFormace['brankar2'] += $koeficientTretina[$i];
            }
        }

        $this->zakladniFormace1->setZapasEnergieZkusenosti($statistiky, $koeficientFormace['formace1'], $koeficientTyputkani);
        $this->zakladniFormace2->setZapasEnergieZkusenosti($statistiky, $koeficientFormace['formace2'], $koeficientTyputkani);
        $this->zakladniFormace3->setZapasEnergieZkusenosti($statistiky, $koeficientFormace['formace3'], $koeficientTyputkani);
        $this->zakladniFormace4->setZapasEnergieZkusenosti($statistiky, $koeficientFormace['formace4'], $koeficientTyputkani);

        $this->brankarFormace->setZapasEnergieZkusenosti($statistiky, new Pozice('brankar', 1), $koeficientFormace['brankar1'], $koeficientTyputkani);
        $this->brankarFormace->setZapasEnergieZkusenosti($statistiky, new Pozice('brankar', 2), $koeficientFormace['brankar2'], $koeficientTyputkani);

        $this->presilovkaFormace1->setZapasEnergieZkusenosti($statistiky, 1, $koeficientTyputkani);
        $this->presilovkaFormace2->setZapasEnergieZkusenosti($statistiky, 2, $koeficientTyputkani);
        $this->oslabeniFormace1->setZapasEnergieZkusenosti($statistiky, 1, $koeficientTyputkani);
        $this->oslabeniFormace2->setZapasEnergieZkusenosti($statistiky, 2, $koeficientTyputkani);
    }

    /**
     * Zjisti aktualni sestavu na zapas
     */
    private function zjistiAktualniSestavu() {
        $this->aktualniSestava = $this->tym->related('sestava', 'tym_id')->where('vychozi', 1)->fetch();
    }
    
    /**
     * Nastavi pravidla
     */
    private function nastavPravidla() {
        $this->taktika->setPravidla($this->aktualniSestava->related('sestavapravidlo', 'sestava_id')); 
    }

    /**
     * Zjisti na kolik se hraje lan
     */
    private function pocetLajn() {
        if ($this->aktualniSestava->triFormace) {
            $this->taktika->setPocetLajn(3);
        } else {
            $this->taktika->setPocetLajn(4);
        }
        $this->taktika->setTriFormaceReakce($this->aktualniSestava->triFormaceReakce);
    }

}
