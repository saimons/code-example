<?php

namespace Model\Calculation\Match;

use Nette\Object;

class Taktika extends Object {

    /**
     * @var \Model\Calculation\Match\UtkaniPrubeh $utkaniPrubeh
     */
    private $utkaniPrubeh;

    /**
     * Na kolik lajn hraje tym
     * @var int
     */
    private $pocetLajn;

    /**
     * @var \Nette\Database\Table\Selection
     */
    private $tym;

    /**
     * Kolik hraci travi na lede
     * @var array
     */
    private $casLed = array();

    /**
     * Pravidla pro jednotlive casti zpasau podle tretin a stavu utkani
     * @var Nette\Database\Table\Selection
     */
    private $sestavapravidlo = array();

    /**
     * Je to tym domaci nebo hoste
     * @var boolen
     */
    private $domaciHoste;

    /**
     * Pravidla, tretina
     * @var array
     */
    private $pravidlo = array(1 => array(), 2 => array(), 3 => array(), 4 => array(), 5 => array());

    /**
     * Reakce na tri formace protihrace
     * @var bool
     */
    private $triFormaceReakce;

    /**
     * @var array
     */
    private $taktikaUtok = array();

    /**
     * @var array
     */
    private $taktikaObrana = array();
    
    /**
     * @var array
     */
    private $taktikaObranaUtok = array();

    /**
     * 
     * @param \Nette\Database\Table\Selection $tym
     */
    public function __construct($tym, $domaciHoste) {
        $this->tym = $tym;
        $this->domaciHoste = $domaciHoste;
    }

    /**
     * @param \Model\Calculation\Match\UtkaniPrubeh $utkaniPrubeh
     */
    public function setUtkaniPrubeh(UtkaniPrubeh $utkaniPrubeh) {
        $this->utkaniPrubeh = $utkaniPrubeh;
    }

    /**
     * Vrati cas na lede pro formaci
     * @return int
     */
    public function getCasLed($cisloFormace) {
        return $this->casLed[$cisloFormace];
    }

    /**
     * Nastavi cas na lede pro jednotlive formace
     * @param int $cisloFormace
     * @param int $hodnota
     */
    public function setCasLed($cisloFormace, $hodnota) {
        $this->casLed[$cisloFormace] = $hodnota;
    }

    /**
     * Nastavi taktiku utoku pro jednotlive formace
     * @param int $cisloFormace
     * @param Nette\Database\Table\ActiveRow $hodnota
     */
    public function setTaktikaUtok($cisloFormace, $hodnota) {
        $this->taktikaUtok[$cisloFormace]['hodnota'] = $hodnota ? : FALSE;
    }
    
    /**
     * Vrati parametry pro zvolenou taktiku na zapas utok
     * @param int $cisloFormace
     * @return Nette\Database\Table\ActiveRow
     */
    public function getTaktikaUtok($cisloFormace) {
        return $this->taktikaUtok[$cisloFormace]['hodnota'];
    }

    /**
     * Nastavi taktiku obrany pro jednotlive formace
     * @param int $cisloFormace
     * @param Nette\Database\Table\ActiveRow $hodnota
     */
    public function setTaktikaObrana($cisloFormace, $hodnota) {
        $this->taktikaObrana[$cisloFormace]['hodnota'] = $hodnota ? : FALSE;
    }
    
    /**
     * Vrati parametry pro zvolenou taktiku na zapas obrana
     * @param int $cisloFormace
     * @return Nette\Database\Table\ActiveRow
     */
    public function getTaktikaObrana($cisloFormace) {
        return $this->taktikaObrana[$cisloFormace]['hodnota'];
    }
    
    /**
     * Nastavi vahu jednotlivym atributum obrana vs. utok
     * @param int $cisloFormace
     * @param Nette\Database\Table\ActiveRow $hodnota
     */
    public function setUtokObrana($cisloFormace, $hodnota) {
        $this->taktikaObranaUtok[$cisloFormace]['hodnota'] = $hodnota ? : FALSE;
    }
    
    /**
     * Vrati vahu jednotlivym atributum obrana vs. utok pro utocniky
     * @param int $cisloFormace
     * @return Nette\Database\Table\ActiveRow
     */
    public function getUtokObranaUtok($cisloFormace) {
        $hodnota = 0.8 + $this->taktikaObranaUtok[$cisloFormace]['hodnota'] * 0.004;
        return $hodnota;
    }
    
    /**
     * Vrati vahu jednotlivym atributum obrana vs. utok pro obrance
     * @param int $cisloFormace
     * @return Nette\Database\Table\ActiveRow
     */
    public function getUtokObranaObrana($cisloFormace) {
        $hodnota = 1.2 - $this->taktikaObranaUtok[$cisloFormace]['hodnota'] * 0.004;
        return $hodnota;
    }

    /**
     * @param int $pocetLajn
     */
    public function setPocetLajn($pocetLajn) {
        $this->pocetLajn = $pocetLajn;
    }

    /**
     * Vrati aktualni pocet lajn
     * @return int
     */
    public function getPocetLajn() {
        $sestavazmenasituace_id = 1;
        $this->vychledejPravidlo($sestavazmenasituace_id);

        if (isset($this->pravidlo[$this->utkaniPrubeh->getTretina()][$sestavazmenasituace_id])) {
            return $this->pravidlo[$this->utkaniPrubeh->getTretina()][$sestavazmenasituace_id]->triFormace;
        } else {
            return $this->pocetLajn;
        }
    }

    /**
     * @param Nette\Database\Table\Selection $sestavapravidlo
     */
    public function setPravidla($sestavapravidlo) {
        $this->sestavapravidlo = $sestavapravidlo;
    }

    /**
     * Vrati aktualni pozici brankare podle casu
     * @param int $tretina
     * @return int
     */
    public function getAktualniPoziceBrankar() {
        $sestavazmenasituace_id = 2;
        $this->vychledejPravidlo($sestavazmenasituace_id);
        if (isset($this->pravidlo[$this->utkaniPrubeh->getTretina()][$sestavazmenasituace_id])) {            
            return $this->pravidlo[$this->utkaniPrubeh->getTretina()][$sestavazmenasituace_id]->brankarPozice;
        } else {
            return 1;
        }
    }

    /**
     * Zjisti v pravidlech jesti aktualni deni neodpovida naka situace pro zmenu v taktice
     * @param int $sestavazmenasituace_id Muze byt jen jedno id pro jednu tretinu
     * @return Nette\Database\Table\ActiveRow
     */
    private function vychledejPravidlo($sestavazmenasituace_id) {
        if (!isset($this->pravidlo[$this->utkaniPrubeh->getTretina()][$sestavazmenasituace_id])) {

            //jaky je rozdil golu
            if ($this->domaciHoste) {
                $aktualniStav = $this->utkaniPrubeh->getGolyDomaci() - $this->utkaniPrubeh->getGolyHoste();
            } else {
                $aktualniStav = $this->utkaniPrubeh->getGolyHoste() - $this->utkaniPrubeh->getGolyDomaci();
            }

            //rozdil golu je preveden na stav situace
            if ($aktualniStav == 0) {
                $situaceStav = 3; //remiza
                $aktualniStav = $this->utkaniPrubeh->getGolyDomaci(); //pokud je remiza rozdil je 0, nastavime pocet padnutych golu
            } elseif ($aktualniStav < 0) {
                $situaceStav = 1; //prohrava
            } else {
                $situaceStav = 2; //vyhrava
            }

            // dump ('tretina '.$this->utkaniPrubeh->getTretina());
            //projdou se vsechny prednastaveni a zjisti se jestli nake odpovida       
            foreach ($this->sestavapravidlo AS $sp) {
                //prasarna kvuli chybe v Nette 2.3.3
                $sp->formace_id;
                $sp->taktika_id;
                $sp->brankarPozice;
                $sp->triFormace;
                //konec prasarny
                if ($sp->sestavazmenasituace_id == $sestavazmenasituace_id
                        AND $sp->situaceStav == $situaceStav
                        AND $sp->rozsahGoluOd <= abs($aktualniStav)
                        AND $sp->rozsahGoluDo >= abs($aktualniStav)
                        AND $sp->situaceTretina + 1 == $this->utkaniPrubeh->getTretina()) {
                    $this->pravidlo[$this->utkaniPrubeh->getTretina()][$sestavazmenasituace_id] = $sp;
                }
            }
        }
    }

    /**
     * @param bool $triFormaceReakce
     */
    public function setTriFormaceReakce($triFormaceReakce) {
        $this->triFormaceReakce = $triFormaceReakce;
    }

    /**
     * @return bool
     */
    public function getTriFormaceReakce() {
        return $this->triFormaceReakce;
    }

}
