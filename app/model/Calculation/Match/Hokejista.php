<?php

namespace Model\Calculation\Match;

use Nette\Object,
    Nette\Database\SqlLiteral;

class Hokejista extends Object {

    /**
     *
     * @var \Nette\Database\Table\selection
     */
    private $hokejista;

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $energie = 0;

    /**
     * @var int
     */
    private $hvezdy = 0;

    /**
     * @var int
     */
    private $forma = 0;

    /**
     * @var int
     */
    private $brana = 0;

    /**
     * @var int
     */
    private $obrana = 0;

    /**
     * @var int
     */
    private $utok = 0;

    /**
     * @var int
     */
    private $sila = 0;

    /**
     * @var int
     */
    private $strela = 0;

    /**
     * @var int
     */
    private $nahravka = 0;

    /**
     * @var int
     */
    private $zkusenosti = 0;

    /**
     * @var int
     */
    private $sebeovladani = 0;

    /**
     * Ovlivneni atributu energii mirou (procentualne)
     * @var float 0-1
     */
    private $energieOvlivneni = 1;

    /**
     * @var int
     */
    private $potencial;

    /**
     * @var int
     */
    private $vek;
    
    /**     
     * @var int
     */
    private $rychlost;

    /**
     * @param \Nette\Database\Table\selection $hokejista
     */
    public function __construct($hokejista = NULL) {
        if ($hokejista) {
            $this->hokejista = $hokejista;
            $this->nastavParametry();
        }
    }

    /**
     * Nastavi tride atributy pro hokejistu
     */
    private function nastavParametry() {
        $this->id = $this->hokejista->id;
        $this->sebeovladani = $this->hokejista->sebeovladani;
        $this->hvezdy = $this->hokejista->hvezdy;
        $this->forma = $this->hokejista->forma;
        $this->brana = $this->hokejista->brana;
        $this->obrana = $this->hokejista->obrana;
        $this->utok = $this->hokejista->utok;
        $this->energie = $this->hokejista->energie;
        $this->sila = $this->hokejista->sila;
        $this->strela = $this->hokejista->strela;
        $this->nahravka = $this->hokejista->nahravka;
        $this->zkusenosti = $this->hokejista->zkusenosti;
        $this->potencial = $this->hokejista->potencial;
        $this->vek = $this->hokejista->vek;
        $this->rychlost = $this->hokejista->rychlost;
        $this->energieOvlivneniAtribut();
    }

    /**
     * Id hokejisty
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    public function getHokejista() {
        return $this->hokejista;
    }

    /**
     * Sebeovladani hrace
     * @return int
     */
    public function getSebeovladani() {
        return $this->sebeovladani * $this->energieOvlivneni;
    }

    /**
     * Hvezdy hrace
     * @return int
     */
    public function getHvezdy() {
        return $this->hvezdy;
    }

    /**
     * Forma hrace
     * @return int
     */
    public function getForma() {
        return $this->forma;
    }

    /**
     * Brana hrace
     * @return int
     */
    public function getBrana() {
        return $this->brana * $this->energieOvlivneni;
    }

    /**
     * Obrana hrace
     * @return int
     */
    public function getObrana() {
        return $this->obrana * $this->energieOvlivneni;
    }

    /**
     * Utok hrace
     * @return int
     */
    public function getUtok() {
        return $this->utok * $this->energieOvlivneni;
    }

    /**
     * Energie hrace
     * @return int
     */
    public function getEnergie() {
        return $this->energie;
    }

    /**
     * Sila hrace
     * @return int
     */
    public function getSila() {
        return $this->sila * $this->energieOvlivneni;
    }

    /**
     * Strela hrace
     * @return int
     */
    public function getStrela() {
        return $this->strela * $this->energieOvlivneni;
    }

    /**
     * Nahravka hrace
     * @return int
     */
    public function getNahravka() {
        return $this->nahravka * $this->energieOvlivneni;
    }

    /**
     * Zkusenosti hrace
     * @return int
     */
    public function getZkusenosti() {
        return $this->zkusenosti;
    }
    
    /**     
     * @return int
     */
    public function getRychlost() {
        return $this->rychlost;
    }

    /**
     * Potencial hrace
     * @return int
     */
    public function getPotencial() {
        return $this->potencial;
    }

    /**
     * Vek hrace
     * @return int
     */
    public function getVek() {
        return $this->vek;
    }

    /**
     * Vytvori miru pro snizeni atributu hokejisty v zavislosti na energii
     */
    private function energieOvlivneniAtribut() {
        if ($this->energie <= 100 AND $this->energie > 75) {
            $this->energieOvlivneni = 1 - ((100 - $this->energie) * 0.002857);
        } elseif ($this->energie <= 75 AND $this->energie > 35) {
            $this->energieOvlivneni = 0.9 - ((75 - $this->energie) * 0.0075);
        } else {
            $this->energieOvlivneni = 0.6 - ((35 - $this->energie) * 0.002857);
        }
    }

    /**
     * Hokejistum prida zkusenosti za zapas a odebere energii !!! pratelak ma pridavat jen polovinu zkusenosti, neni udelano !!!
     * @param float $koeficient kolik je pridano
     * @param int $casLed {30,70}
     * @param int $presilovka
     * @param int $oslabeni
     */
    public function updateHokejistaPoZapase($statistiky, $koeficient, $koeficientTyputkani, $casLed = 70, $presilovka = 0, $oslabeni = 0) {
        if (!$presilovka AND ! $oslabeni) {
            $trenZK = ((0.25 + (self::nahoda() * 0.1)) * (($casLed * 0.01) + 0.3)) * $koeficient * $koeficientTyputkani;
            if ($casLed == 0) {
                $trenZK = 0;
            }
            $energie = round((((($casLed - 30) * 10) / 40) + 20 + rand(0, 4)) * $koeficient);
        } elseif ($presilovka AND ! $oslabeni) {
            $trenZK = round(((0.25 + (self::nahoda() * 0.1)) * $presilovka == 1 ? 0.2 : 0.12) * $koeficientTyputkani);
            $energie = 2;
        } elseif (!$presilovka AND $oslabeni) {
            $trenZK = round(((0.25 + (self::nahoda() * 0.1)) * $oslabeni == 1 ? 0.18 : 0.13) * $koeficientTyputkani);
            $energie = 3;
        }
//        \Tracy\Debugger::barDump($koeficient);
        $this->energie -= $energie;
        if ($this->energie < 0) {
            $this->energie = 0;
        }
        if (!DEBUG) {
            if (isset($this->hokejista->id)) { //pozdeji by se mohlo dat odstranit (podminkia), protoze budou mit vsechny petky hokejisty
                $this->hokejista->update(array(
                    'pocetUtkaniLiga' => new SqlLiteral('pocetUtkaniLiga + 1'),
                    'zkusenosti' => new SqlLiteral('zkusenosti + ' . $trenZK),
                    'zkusenostiTr' => (!$presilovka AND ! $energie) ? $trenZK : new SqlLiteral('zkusenostiTr + ' . $trenZK),
                    'energie' => $this->energie,
                    'energieTr' => (!$presilovka AND ! $energie) ? $energie : new SqlLiteral('0 - ' . $energie)
                ));
            }
        }
        if (isset($this->hokejista->id)) {            
            $statistiky->updateHokejistaEnergie($this->hokejista->id, $energie);
        }
    }

    /**
     * Nastavi do DB hokejistovi zraneni
     * @param int $pocet
     */
    public function updateHokejistaZraneni($pocet) {
        if (!DEBUG) {
            if (isset($this->hokejista->id)) { //pozdeji by se mohlo dat odstranit, protoze budou mit vsechny petky hokejisty
                $this->hokejista->update(array(
                    'zraneni' => $pocet
                ));
            }
        }
    }

    /**
     * Vygeneruje nahodne clislo mezi 0-1
     * @return float
     */
    private static function nahoda() {
        return mt_rand() / mt_getrandmax();
    }

}
