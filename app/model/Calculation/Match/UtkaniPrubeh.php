<?php

namespace Model\Calculation\Match;

use Nette\Object;

/**
 * Model pro vypocet zapasu
 */
class UtkaniPrubeh extends Object {

    const MAX_CAS_UTKANI = 4201; //maximalni cas utkani ktereho muze dosahnout

    /**
     * Cas prave probihajiciho utkani
     * @var int
     */

    private $casUtkani = 0;

    /**
     * Nuluje se pokud zacina tretin, skonci presilovka/oslabeni
     * @var int
     */
    private $casRotace = 0;

    /**
     * Nuluje se pokud zacina tretin, skonci presilovka/oslabeni
     * @var int
     */
    private $casRotaceVylouceni = 0;

    /**
     * Udalost kdy nastane dalsi situace
     * @var int
     */
    private $dalsiUdalost = 0;

    /**
     * Pocet golu domacich celkem
     * @var int
     */
    private $golyDomaci = 0;

    /**
     * @var int
     */
    private $golyHoste = 0;

    /**
     * @var bool
     */
    private $prodlouzeni = FALSE;

    /**
     * @var bool
     */
    private $najezdy = FALSE;

    /**
     * parametr pro zjisteni jaka probiha tretina, pokud jedna skonci tak se u ni nastavi TRUE
     * @var int 
     */
    private $tretina = 0;

    /**
     * Ulozeni jak se hralo v jednotlive tretine, pocet lajn
     * @var array
     */
    private $triFormace = array('domaci' => array(), 'hoste' => array());

    /**
     * Ulozeni jak se hralo v jednotlive tretine, jaky brankar
     * @var array
     */
    private $brankarPozice = array('domaci' => array(), 'hoste' => array());

    /**
     * Provizorni promena pro setr pocet lajn
     * @var array
     */
    private $pocetLajn = array('domaci' => NULL, 'hoste' => NULL);

    /**
     * Provizorni promena pro setr lajna brankar
     * @var array
     */
    private $brankarLajna = array('domaci' => NULL, 'hoste' => NULL);

    /**
     * Typ utkani
     * @var int
     */
    private $typutkani_id;
    
    /**
     * Jestli se proi danou tretinu nacetli nove zmeny
     * @var array
     */
    private $zmenaPravidlaTretina = array(1 => FALSE, 2 => FALSE, 3 => FALSE, 4 => FALSE, 5 => FALSE);

    /**
     * 
     */
    public function __construct() {
        $this->dalsiUdalost = (4 + (mt_rand() / mt_getrandmax()) * 30); //v jakem pripoctu k casuUtkani se stane prvni nasledujici udalost
        //$this->casUtkani = rand(1, 10); //v jake sekunde se stane prvni udalost !!!opraveno jinak presunutim funkce na inkrementaci casu na zacatek 
    }

    /**
     * @return int
     */
    public function getCasUtkani() {
        return $this->casUtkani;
    }

    /**
     * Vrati aktualni tretinu
     * @return int
     */
    public function getTretina() {
        return $this->tretina;
    }

    /**
     * Vrati jak se hralo v jednotlivych formacich pocet lajn
     * @return array
     */
    public function getTriFormace() {
        return $this->triFormace;
    }

    /**
     * Vrati jak se hralo v jednotlivych formacich, ktery brankar
     * @return array
     */
    public function getBrankarPozice() {
        return $this->brankarPozice;
    }

    /**
     * @return int
     */
    public function getMaxCasUtkani() {
        return self::MAX_CAS_UTKANI;
    }

    /**
     * @return int
     */
    public function getDalsiUdalost() {
        return $this->dalsiUdalost;
    }

    /**
     * @return bool
     */
    public function getProdlouzeni() {
        return $this->prodlouzeni;
    }

    /**
     * @return bool
     */
    public function getNajezdy() {
        return $this->najezdy;
    }

    /**
     * @return int
     */
    public function getTyputkani_id() {
        return $this->typutkani_id;
    }

    /**
     * @param int $typutkani_id
     */
    public function setTyputkani_id($typutkani_id) {
        $this->typutkani_id = $typutkani_id;
    }

    /**
     * 
     */
    public function setProdlouzeni() {
        $this->prodlouzeni = TRUE;
    }

    /**
     * 
     */
    public function setNajezdy() {
        $this->najezdy = TRUE;
    }

    /**
     * Vrati cas nasledujci udalsoti
     * @return int
     */
    public function createDalsiUdalost() {
        $this->dalsiUdalost = (8 + (mt_rand() / mt_getrandmax()) * 42);
    }

    /**
     * @return int
     */
    public function getGolyDomaci() {
        return $this->golyDomaci;
    }

    /**
     * Incremetuje goly domaci
     */
    public function setGolyDomaci() {
        $this->golyDomaci ++;
    }

    /**
     * Incremetuje goly hoste
     */
    public function setGolyHoste() {
        $this->golyHoste ++;
    }

    /**
     * @return int
     */
    public function getGolyHoste() {
        return $this->golyHoste;
    }

    /**
     * Nastavi cas rotace
     * @param int $time
     */
    public function setCasRotace($time) {
        $this->casRotace = $time;
    }

    /**
     * Nastavi se cas rotace vylouceni
     * @param int $time
     */
    public function setCasRotaceVylouceni($time) {
        $this->casRotaceVylouceni = $time;
    }

    /**
     * Incrementuje cas zapasu
     * Incrementuje cas rotaci formaci, pokud jedna skonci nastavi se na nulu, tim se zajisti nasazeni prvnich formaci proti sobe
     */
    public function incrementCas() {
        //dump ('tretina utkani prubeh ', $this->tretina);
        if ($this->casUtkani >= 0 AND $this->tretina == 0) {
            $this->tretina = 1;
            $this->casRotace = 0;            
            $this->triFormace['domaci'][$this->tretina] = $this->pocetLajn['domaci'];
            $this->triFormace['hoste'][$this->tretina] = $this->pocetLajn['hoste'];
            $this->brankarPozice['domaci'][$this->tretina] = $this->brankarLajna['domaci'];
            $this->brankarPozice['hoste'][$this->tretina] = $this->brankarLajna['hoste'];
        } elseif ($this->casUtkani > 1200 AND $this->tretina == 1) {
            dump('stav ppo 1 tretine:' . $this->golyDomaci . ':' . $this->golyHoste);
            $this->tretina = 2;
            $this->casRotace = 0;
        } elseif ($this->casUtkani > 2400 AND $this->tretina == 2) {
            $this->tretina = 3;
            $this->casRotace = 0;
        } elseif ($this->casUtkani > 3600 AND $this->tretina == 3) {
            $this->tretina = 4;
            $this->casRotace = 0;
        } elseif ($this->casUtkani > 4200 AND $this->tretina == 4) {
            $this->tretina = 5;
            $this->casRotace = 0;
        }

        $this->casUtkani += $this->dalsiUdalost;
        $this->casRotace += $this->dalsiUdalost;
        $this->casRotaceVylouceni += $this->dalsiUdalost;
    }
    
    /**
     * Nastavuji se pravidla podle zapasovych sestavapravidlo
     */
    public function zmenyPravidla() {
        if ($this->tretina == 2 AND !$this->zmenaPravidlaTretina[$this->tretina]) {            
            $this->triFormace['domaci'][$this->tretina] = $this->pocetLajn['domaci'];
            $this->triFormace['hoste'][$this->tretina] = $this->pocetLajn['hoste'];
            $this->brankarPozice['domaci'][$this->tretina] = $this->brankarLajna['domaci'];
            $this->brankarPozice['hoste'][$this->tretina] = $this->brankarLajna['hoste'];
            $this->zmenaPravidlaTretina[$this->tretina] = TRUE;
        } elseif ($this->tretina == 3 AND !$this->zmenaPravidlaTretina[$this->tretina]) {
            $this->triFormace['domaci'][$this->tretina] = $this->pocetLajn['domaci'];
            $this->triFormace['hoste'][$this->tretina] = $this->pocetLajn['hoste'];
            $this->brankarPozice['domaci'][$this->tretina] = $this->brankarLajna['domaci'];
            $this->brankarPozice['hoste'][$this->tretina] = $this->brankarLajna['hoste'];
            $this->zmenaPravidlaTretina[$this->tretina] = TRUE;
        } elseif ($this->tretina == 4 AND !$this->zmenaPravidlaTretina[$this->tretina]) {
            $this->triFormace['domaci'][$this->tretina] = $this->pocetLajn['domaci'];
            $this->triFormace['hoste'][$this->tretina] = $this->pocetLajn['hoste'];
            $this->brankarPozice['domaci'][$this->tretina] = $this->brankarLajna['domaci'];
            $this->brankarPozice['hoste'][$this->tretina] = $this->brankarLajna['hoste'];
            $this->zmenaPravidlaTretina[$this->tretina] = TRUE;
        } elseif ($this->tretina == 5 AND !$this->zmenaPravidlaTretina[$this->tretina]) {
            $this->triFormace['domaci'][$this->tretina] = $this->pocetLajn['domaci'];
            $this->triFormace['hoste'][$this->tretina] = $this->pocetLajn['hoste'];
            $this->brankarPozice['domaci'][$this->tretina] = $this->brankarLajna['domaci'];
            $this->brankarPozice['hoste'][$this->tretina] = $this->brankarLajna['hoste'];
            $this->zmenaPravidlaTretina[$this->tretina] = TRUE;
        }
    }

    /**
     * 
     * @param array $pocetLajn
     */
    public function setPocetLajn($pocetLajn) {
        $this->pocetLajn = $pocetLajn;
    }

    /**
     * 
     * @param bool $domaciHoste
     * @param int $pocetLajn
     */
    public function setBrankarLajna($brankarLajna) {
        $this->brankarLajna = $brankarLajna;
    }

    /**
     * Cas rotaci
     * @return int
     */
    public function getCasRotace() {
        return $this->casRotace;
    }

    /**
     * Cas rotaci
     * @return int
     */
    public function getCasRotaceVylouceni() {
        return $this->casRotaceVylouceni;
    }

}
