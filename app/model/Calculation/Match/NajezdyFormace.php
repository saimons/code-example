<?php

namespace Model\Calculation\Match;

use Nette\Object;

class NajezdyFormace extends Formace {

    /**
     * @param int $formace
     * @param \Model\Calculation\Match\Taktika $taktika
     */
    public function __construct($formace, Taktika $taktika = NULL) {
        parent::__construct($formace, $taktika);
    }

    /**
     * 
     * @param \Model\Calculation\Match\Pozice $pozice
     * @return int
     */
    public function getParametryNajezdStrela(Pozice $pozice) {
        $hokejista = $this->getHracPozice($pozice);
        return $hokejista->strela + self::KO_SN_STRELA_UTOK * $hokejista->utok + self::KO_SN_STRELA_ZKUSENOSTI * $hokejista->zkusenosti;
    }

}
