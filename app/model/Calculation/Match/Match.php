<?php

namespace Model\Calculation\Match;

use Nette\Object,
    Model\Calculation\ICalculation,
    Model\Database\Interfaces\IPrepocetdennitymy,
    Model\Database\Interfaces\IRadarealna,
    Model\Database\Interfaces\IUtkani,
    Model\Database\Interfaces\INovinka,
    Model\Database\Interfaces\IPrepocetdennizranenitymy,
    Model\Database\Interfaces\IStatistikagolmaniligovautkani,
    Model\Database\Interfaces\ITabulkaplayout,
    Model\Database\Interfaces\IUdalost,
    Model\Database\Interfaces\IUdalostsestava,
    Model\Database\Interfaces\IPenize,
    Model\Training\TrainingModel;

/**
 * Model pro inicializaci CRONU
 */
class Match extends Object implements ICalculation {

    /**
     * @var \Model\Database\Interfaces\IPrepocetdennitymy
     */
    public $prepocetdennitymy;

    /**
     * @var \Model\Database\Interfaces\IRadarealna
     */
    public $radarealna;

    /**
     * @var \Model\Database\Interfaces\IUtkani
     */
    private $utkani;

    /**
     * @var \Model\Database\Interfaces\INovinka
     */
    private $novinka;

    /**
     *
     * @var \Model\Database\Interfaces\IPrepocetdennizranenitymy
     */
    private $prepocetdennizranenitymy;

    /**
     *
     * @var \Model\Database\Interfaces\IStatistikagolmaniligovautkani
     */
    private $statistikagolmaniligovautkani;

    /**
     * @var \Model\Database\Interfaces\ITabulkaplayout
     */
    private $tabulkaplayout;

    /**
     * @var \Model\Database\Interfaces\IUdalost
     */
    private $udalost;

    /**
     * @var \Model\Database\Interfaces\IUdalostsestava
     */
    private $udalostsestava;

    /**
     * @var \Model\Database\Interfaces\IPenize
     */
    private $penize;

    /**
     * @var \RadaRealnaModel 
     */
    private $mRadarealna;

    /**
     * @var Model\Training\TrainingModel
     */
    private $mTraining;

    /**
     * 
     * @param IPrepocetdennitymy $prepocetdennitymy
     * @param IRadarealna $radarealna
     */
    public function __construct(
    IPrepocetdennitymy $prepocetdennitymy, IRadarealna $radarealna, IUtkani $utkani, INovinka $novinka, IPrepocetdennizranenitymy $prepocetdennizranenitymy, IStatistikagolmaniligovautkani $statistikagolmaniligovautkani, ITabulkaplayout $tabulkaplayout, IUdalost $udalost, IUdalostsestava $udalostsestava, IPenize $penize, \RadaRealnaModel $mRadarealna, TrainingModel $mTraining
    ) {
        $this->prepocetdennitymy = $prepocetdennitymy;
        $this->radarealna = $radarealna;
        $this->utkani = $utkani;
        $this->novinka = $novinka;
        $this->prepocetdennizranenitymy = $prepocetdennizranenitymy;
        $this->statistikagolmaniligovautkani = $statistikagolmaniligovautkani;
        $this->tabulkaplayout = $tabulkaplayout;
        $this->udalost = $udalost;
        $this->udalostsestava = $udalostsestava;
        $this->penize = $penize;
        $this->mRadarealna = $mRadarealna;
        $this->mTraining = $mTraining;
    }

    /**
     * Spusteni prepoctu zapasu
     */
    public function run() {
        $this->getTymyPrepocet();
    }

    /**
     * Zjisti z DB tymy pro prepocet
     */
    private function getTymyPrepocet() {
        if (DEBUG) {
            $utkani = $this->utkani->create()->findAll()->get(574028);
            $this->vypoctiUtkani($utkani);
            die();
        }

        //overeni tymu kterym vygenerovat nahodnou sestavu
        $nahodnaSestava = $this->radarealna->create()->findAll()->select('DISTINCT tym_id')->where('hokejista_id', 0)->where('stav <> ?', 100);
        foreach ($nahodnaSestava AS $ns) {
            $this->mRadarealna->tym_id = $ns->tym_id; //prednastavi tym v rada realna
            $this->mRadarealna->prepoctiRady('c'); //vytvori nahodnou sestavu
        }

        $prepocetdennitymy = $this->prepocetdennitymy->create()->findAll();
        foreach ($prepocetdennitymy AS $prepocetdennitym) {
            $this->vypoctiVsechnyUtkaniTym($prepocetdennitym); //vypocte se utkani
            $this->mTraining->finishTrainingFromCRON($prepocetdennitym->tym_id); //udela se 6h treninkovy slot            
            $prepocetdennitym->delete();
        }
    }

    /**
     * Vypocita vsechny utkani
     * @param \Nette\Database\Table\Selection $prepocetdennitym
     */
    private function vypoctiVsechnyUtkaniTym($prepocetdennitym) {
        $vsechnyUtkani = $this->utkani->create()->findAll()->where('tymTomaci_id', $prepocetdennitym->tym_id)->where('datumCas < NOW() AND (divaci IS NULL) AND ((tymVyzyvany_id IS NULL) OR ((tymVyzyvany_id IS NOT NULL) AND (akceptovano = 1)))');
        foreach ($vsechnyUtkani AS $utkani) {
            $this->vypoctiUtkani($utkani);
        }
        return 1;
    }

    /**
     * Zavola vypocet jednoho utkani
     * @param \Nette\Database\Table\Selection $utkani
     */
    private function vypoctiUtkani($utkani) {
        $tymT = $utkani->ref('tym', 'tymTomaci_id');
        $tymTomaci = new Tym($tymT, 1, $this->radarealna->create()->findBy(['tym_id' => $tymT->id]));
        $tymH = $utkani->ref('tym', 'tymHoste_id');
        $tymHoste = new Tym($tymH, 0, $this->radarealna->create()->findBy(['tym_id' => $tymH->id]));

        $divaci = new Divaci($utkani, $tymTomaci, $tymHoste);
        $divaci->run();

        if (!DEBUG) {
            $utkani->update(array('divaci' => $divaci->getPocetDivaku(), 'vstupne' => $divaci->getVstupne()));
            $tymT->update(array('hotovost' => new \Nette\Database\SqlLiteral('hotovost + ' . $divaci->getPenizeTomaci())));
            $tymH->update(array('hotovost' => new \Nette\Database\SqlLiteral('hotovost + ' . $divaci->getPenizeHoste())));
            $this->penize->create()->findAll()->insert(array(
                'tym_id' => $tymT->id,
                'penizetyp_id' => 3,
                'casDatum' => new \DateTime(),
                'castka' => $divaci->getPenizeTomaci()
            ));

            $this->penize->create()->findAll()->insert(array(
                'tym_id' => $tymH->id,
                'penizetyp_id' => 3,
                'casDatum' => new \DateTime(),
                'castka' => $divaci->getPenizeHoste()
            ));
        }



        //vypocet utkani
        $utkaniT = new Utkani($utkani, $tymTomaci, $tymHoste);
        $utkaniT->run();

        if (!DEBUG) {
            //ulozeni zraneni
            $date = new \DateTime();
            foreach ($utkaniT->getZraneni() AS $zraneni) {
                $this->novinka->create()->findAll()->insert(array(
                    'tym_id' => $zraneni['tym_id'],
                    'novinkatyp_id' => 1,
                    'datum' => $date,
                    'refID' => $zraneni['hokejista_id']
                ));
                $this->prepocetdennizranenitymy->create()->findAll()->insert(array(
                    'tym_id' => $zraneni['tym_id'],
                ));
            }


            //ulozeni statistik golmani
            $statistikagolmaniligovautkani = $utkaniT->getStatisikyGolmani();
            $this->statistikagolmaniligovautkani->create()->findAll()->insert($statistikagolmaniligovautkani);



            //uloziudalost a udalost sestava
            $udalost = $utkaniT->getUdalost();
            $udalostSestava = $utkaniT->getUdalostsestava();

            $this->udalost->create()->findAll()->insert($udalost);
            $this->udalostsestava->create()->findAll()->insert($udalostSestava);
        }
        return 1;
    }

}
