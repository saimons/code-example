<?php

namespace Model\Calculation\Match;

use Nette\Object;

class OslabeniFormace extends Formace {
    
    /**
     * Jestli se jedna o dvojnasobne vylouceni
     * @var bool
     */
    private $dvojita;
    
    /**
     * Jsou ulozeny nahradnici na oslabeni, pokud byl vyloucen hrac, ktery ho ma hrat
     * @var array 
     */
    private $nahradnikOslabeni;
    
    /**
     * @var array
     */
    private $vylouceni;


    /** 
     * @param int $formace
     * @param \Model\Calculation\Match\Taktika $taktika
     */
    public function __construct($formace, Taktika $taktika = NULL) {
        parent::__construct($formace, $taktika);
    }
    
    /**     
     * @param bool $dvojita
     */
    public function setDvojita($dvojita) {
        $this->dvojita = $dvojita;
    }
    
    /**     
     * @param \Model\Calculation\Match\Vylouceni $vylouceni
     */
    public function setVylouceni($vylouceni) {
        $this->vylouceni = $vylouceni;
    }

        /**     
     * @param array $nahradnikOslabeni
     */
    public function setNahradnikOslabeni($nahradnikOslabeni) {
        $this->nahradnikOslabeni = $nahradnikOslabeni;
    }
    
    /**
     * Vrati sestavu na oslabeni
     * @return array
     */
    public function getSestava() {
        return $this->pripravSestavu();
    }
    
    /**
     * Pripravi sestavy pro oslabeni, vyhazi hrace vyloucene a nahradi je
     */
    private function pripravSestavu() {
        $nahrazeni = FALSE;
        $sestava = $this->sestava;
        foreach ($sestava AS $pozice => $hokejista) {
            // pokud jsou dva vylouceni 3:5, vyradi se vylouceni hraci ze sestavy
            if ($this->dvojita) {                
                if ($pozice == 'RW') {
                    $sestava[$pozice] = new Hokejista();                    
                }
            }
            //pokud byl prvni hrac vyloucen a je soucasti petky na oslabeni nahradi se nahradnikem
            if (isset($this->vylouceni[$hokejista->id]) AND !$nahrazeni) {
                switch ($pozice) {
                    case 'RW':
                    case 'C':
                        \Tracy\Debugger::barDump($this->nahradnikOslabeni['W']);
                        $sestava[$pozice] = $this->nahradnikOslabeni['W'][$this->formace - 1];
                        break;
                    case 'RD':
                    case 'LD':
                        $sestava[$pozice] = $this->nahradnikOslabeni['D'][$this->formace - 1];
                        break;
                }
                $nahrazeni = TRUE;
            }
        }
        return $sestava;
    }
    
    /**
     * Updejtuje na konci zapasu energie a zkusenosti pro hrace v presilovce
     * @param float $oslabeni
     */
    public function setZapasEnergieZkusenosti($statistiky, $oslabeni, $koeficientTyputkani) {        
        foreach ($this->sestava AS $hokejista) {
            $hokejista->updateHokejistaPoZapase($statistiky, 0, $koeficientTyputkani, 0, 0, $oslabeni);
        }
    }
   
    
    
}



