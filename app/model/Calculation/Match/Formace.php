<?php

namespace Model\Calculation\Match;

use Nette\Object;

class Formace extends Object {

    //kdo ziskal puk
    const KO_UTOK_SILA = 0.3,
            KO_UTOK_ZKUSENOSTI = 0.3,
            KO_UTOK_NAHRAVKA = 0.5,
            KO_OBRANA_ZKUSENOSTI = 0.3;
    //prechod do utoku
    const KO_STRELA_POZICE_OBRANA = 0.3,
            KO_STRELA_POZICE_NAHRAVKA = 0.3,
            KO_STRELA_POZICE_ZKUSENOSTI = 0.3,
            KO_STRELA_POZICE_SILA = 0.3;
    //Strela na branku
    const KO_STRELA_UTOK = 0.25,
            KO_STRELA_NAHRAVKA = 0.25,
            KO_STRELA_ZKUSENOSTI = 0.4,
            KO_BRANA_GLOBAL = 8,
            KO_BRANA_SILA = 0.5,
            KO_BRANA_ZKUSENOSTI = 0.4,
            KO_BRANA_SEBEOVLADANI = 0.4;
    //Smostatny najezd
    const KO_SN_STRELA_UTOK = 0.5,
            KO_SN_STRELA_ZKUSENOSTI = 0.5,
            KO_SN_BRANA_SILA = 0.4,
            KO_SN_BRANA_SEBEOVLADANI = 0.25,
            KO_SN_BRANA_ZKUSENOSTI = 0.3;

    /**
     * Cislo/poradi formace
     * @var int
     */
    protected $formace;

    /**
     * @var Taktika
     */
    protected $taktika;

    /**
     *
     * @var type 
     */
    protected $sestava;

    /**
     * @param int $formace
     */
    public function __construct($formace, Taktika $taktika = NULL) {
        $this->formace = $formace;
        $this->taktika = $taktika;
        for ($i = 1; $i <= 5; $i++) {
            $pozice = new Pozice('zakladni', $i);
            $this->sestava[$pozice->getPozice()] = new Hokejista();
        }
    }

    /**
     * Vrati poradi formace
     * @return int
     */
    public function getPoradiFormace() {
        return $this->formace;
    }

    /**
     * 
     * @param Hokejista $hokejista
     * @param Pozice $pozice
     */
    public function setHokejista(Hokejista $hokejista, Pozice $pozice) {
        $this->sestava[$pozice->getPozice()] = $hokejista;
    }

    /**
     *
     * @return array
     */
    public function getSestava() {
        return $this->sestava;
    }

    /**
     * Vrati atributy pro sanci, ktere ovlivnuji vylouceni D/H
     * @return float
     */
    public function getParametryVylouceni() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->sebeovladani;
        }
        return $parametr;
    }

    /**
     * Vrati sanci na utok pro porovnani utok D/H pokud se nehraje 5:5
     * @return float
     */
    public function getParametryUtok() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->obrana +  self::KO_UTOK_SILA + $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI +  + $hokejista->nahravka * self::KO_UTOK_NAHRAVKA;
        }
        return $parametr;
    }

    /**
     * Zjisti se jestli se utok povedl nebo se ho podarilo zablokovat, parametry pro utocici tym pokud se nehraje 5:5
     * @return float
     */
    public function getParametryStrelaBlokUtok() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->utok + $hokejista->nahravka + $hokejista->zkusenosti * self::KO_UTOK_ZKUSENOSTI;
        }
        return $parametr;
    }

    /**
     * Zjisti se jestli se utok povedl nebo se ho podarilo zablokovat, parametry pro branici tym pokud se nehraje 5:5
     * @return float
     */
    public function getParametryStrelaBlokObrana() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->obrana + $hokejista->sila + $hokejista->zkusenosti * self::KO_OBRANA_ZKUSENOSTI;
        }
        return $parametr;
    }

    /**
     * Vrati parametr pro sanci streli podle pozice
     * @param \Model\Calculation\Match\Pozice $pozice
     * @return int
     */
    public function getParametrPoziceStrela(Pozice $pozice) {
        $hokejista = $this->getHracPozice($pozice);
        return $hokejista->utok + $ + $hokejista->nahravka * self::KO_STRELA_POZICE_NAHRAVKA + $hokejista->zkusenosti * self::KO_STRELA_POZICE_ZKUSENOSTI + $hokejista->sila * self::KO_STRELA_POZICE_SILA;
    }

    /**
     * Parametr pro strelu na branku
     * @param \Model\Calculation\Match\Pozice $pozice
     * @return float
     */
    public function getParametryStrelaUtok(Pozice $pozice) {
        $hokejista = $this->getHracPozice($pozice);
        return $hokejista->strela + $hokejista->utok * self::KO_STRELA_UTOK  + $hokejista->zkusenosti * self::KO_STRELA_ZKUSENOSTI;
    }

    /**
     * Vrati hrace podle pozice
     * @param Pozice $pozeice
     * @return Hokejista
     */
    public function getHracPozice(Pozice $pozeice) {
        return $this->sestava[$pozeice->getPozice()];
    }

    //smazat//
    public function getZkusenosti() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->zkusenosti;
        }
        return $this->taktika->getPocetLajn() == 3 ? $parametr * 5 / 3 : $parametr;
    }

    //smazat//
    public function getZrucnost() {
        $parametr = 0;
        foreach ($this->sestava AS $hokejista) {
            $parametr += $hokejista->brana + $hokejista->utok + $hokejista->obrana + $hokejista->strela + $hokejista->sila + $hokejista->nahravka + $hokejista->sebeovladani;
        }
        return $this->taktika->getPocetLajn() == 3 ? $parametr * 7 / 3 : $parametr;
    }

}
