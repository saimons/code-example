<?php

namespace Model\Calculation\Match;

use Nette\Object;

/**
 * Description of Vylouceni
 *
 * @author stepan
 */
class Vylouceni extends Object {

    /**
     * Pokud je hrac vyloucen nastavi se doba vylouceni | ID hrace (INT) => cas (INT)
     * @var int
     */
    private $vylouceniD = array();

    /**
     * Pokud je hrac vyloucen nastavi se doba vylouceni
     * @var int
     */
    private $vylouceniH = array();

    public function __construct() {
        
    }

    /**
     * Je nekdo vyloucen
     * @return array
     */
    public function isVylouceni() {
        if (count($this->vylouceniD) OR count($this->vylouceniH)) {
            return (object) array('domaci' => $this->vylouceniD, 'hoste' => $this->vylouceniH);
        } else {
            return (object) array('domaci' => NULL, 'hoste' => NULL);
        }
    }

    /**
     * Odebere vylouceneho hrace tymu ktery dostal gol
     * @param bool $domaciHoste 1-domaci,0-hoste; komu odebere hrace z vyloucenych
     */
    public function golVylouceni($domaciHoste) {
        if ($domaciHoste) {
            $this->vylouceniD = $this->odeberVylouceneho($this->isVylouceni()->domaci);
        } else {
            $this->vylouceniH = $this->odeberVylouceneho($this->isVylouceni()->hoste);
        }
    }

    /**
     * Najde klic nejdrive vylouceneho
     * @param array $vylouceni
     * @return arra
     */
    private function odeberVylouceneho($vylouceni) {
        if (is_array($vylouceni) AND count($vylouceni)) {
            foreach ($vylouceni AS $k => $v) {
                $key = $k;
                break;
            }
            unset($vylouceni[$key]);
        }
        return $vylouceni;
    }

    /**
     * Nastavi se vylouceni
     * @param Hokejista $hokejista
     * @param int $cas
     * @param bool $hosteDomaci 0-hoste, 1-domaci
     */
    public function setVylouceni($hokejista, $cas, $hosteDomaci) {
        if ($hokejista instanceof Hokejista) {
            if ($hosteDomaci) {
                $this->vylouceniD[$hokejista->id] = array('cas' => $cas, 'hokejista' => $hokejista);
            } else {
                $this->vylouceniH[$hokejista->id] = array('cas' => $cas, 'hokejista' => $hokejista);
            }
        }
    }

    /**
     * Dekrementuje vylouceni pro jednotlive vyloucene hrace
     * @param int $dalsiUdalost
     * @return boolean Vynulovat cas rotace?
     */
    public function decremetVylouceni($dalsiUdalost) {
        if (is_array($this->vylouceniD)) {
            foreach ($this->vylouceniD AS $key => $vylouceni) {
                $this->vylouceniD[$key]['cas'] -= $dalsiUdalost;
                if ($this->vylouceniD[$key]['cas'] <= 0) {
                    unset($this->vylouceniD[$key]);
                    return TRUE;
                }
            }
        }
        if (is_array($this->vylouceniH)) {
            foreach ($this->vylouceniH AS $key => $vylouceni) {
                $this->vylouceniH[$key]['cas'] -= $dalsiUdalost;
                if ($this->vylouceniH[$key]['cas'] <= 0) {
                    unset($this->vylouceniH[$key]);
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

}
