<?php

namespace Model\Calculation\Match;


class PresilovkaFormace extends Formace {
    
     

    /**
     * @param int $formace
     * @param \Model\Calculation\Match\Taktika $taktika
     */
    public function __construct($formace, Taktika $taktika = NULL) {
        parent::__construct($formace, $taktika);
    }
    
    /**
     * Updejtuje na konci zapasu energie a zkusenosti pro hrace v presilovce
     * @param float $presilovka
     */
    public function setZapasEnergieZkusenosti($statistiky, $presilovka, $koeficientTyputkani) {
        foreach ($this->sestava AS $hokejista) {
            $hokejista->updateHokejistaPoZapase($statistiky, 0, $koeficientTyputkani, 0, $presilovka, 0);
        }
    }
}

