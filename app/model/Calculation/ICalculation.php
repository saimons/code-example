<?php

namespace Model\Calculation;

/**
 *
 * @author stepan
 */
interface ICalculation {
    
    /**
     * Spusteni prepoctu
     */
    public function run();
}
