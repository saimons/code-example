<?php

namespace App\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MatchCalculationCommand extends Command {

    protected function configure() {
        $this->setName('app:matchcalc')
                ->setDescription('Match Calculation');
    }

    /**
     * @param \Model\Calculation\Base\Junction $juniction
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $juniction = $this->getHelper('container')->getByType('\Model\Calculation\Base\Junction');
        $return = $juniction->initCron();
        $output->writeLn($return);
    }

}
